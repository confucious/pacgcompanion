Pathfinder Adventure Card Game Companion
=======

iOS app to track character feat progression and saved cards between scenarios.

Also tracks removed cards from scenario and offers a randomizer that takes all of the above into account.

Environment Setup
=======

Highly recommended to make this git configuration to avoid dangerous `git push` behavior.

`git config --global push.default nothing`

Install CocoaPods if you don't have it yet.

`sudo gem install cocoapods`

As pods are added or updated, you'll need to run:

`pod install`
