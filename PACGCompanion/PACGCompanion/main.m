//
//  main.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/8/13.
//
//

#import <UIKit/UIKit.h>

#import "PACAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PACAppDelegate class]));
    }
}
