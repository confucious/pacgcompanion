//
//  PACCharacterProperty.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/13/13.
//
//

#import "PACCharacterProperty.h"
#import "PACCharacter.h"
#import "PACCharacterFeat.h"


@implementation PACCharacterProperty

@dynamic configId;
@dynamic effect;
@dynamic name;
@dynamic optional;
@dynamic active;
@dynamic character;
@dynamic feats;

@end
