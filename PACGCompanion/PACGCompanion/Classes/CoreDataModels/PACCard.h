//
//  PACCard.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PACCharacter;

@interface PACCard : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic) BOOL removedFromGame;
@property (nonatomic, retain) NSString * set;
@property (nonatomic, retain) PACCharacter *owner;

@end
