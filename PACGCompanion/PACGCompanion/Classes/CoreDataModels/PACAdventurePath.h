//
//  PACAdventurePath.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PACCharacter;

@interface PACAdventurePath : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSOrderedSet *adventures;
@property (nonatomic, retain) PACCharacter *character;
@end

@interface PACAdventurePath (CoreDataGeneratedAccessors)

- (void)insertObject:(NSManagedObject *)value inAdventuresAtIndex:(NSUInteger)idx;
- (void)removeObjectFromAdventuresAtIndex:(NSUInteger)idx;
- (void)insertAdventures:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeAdventuresAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInAdventuresAtIndex:(NSUInteger)idx withObject:(NSManagedObject *)value;
- (void)replaceAdventuresAtIndexes:(NSIndexSet *)indexes withAdventures:(NSArray *)values;
- (void)addAdventuresObject:(NSManagedObject *)value;
- (void)removeAdventuresObject:(NSManagedObject *)value;
- (void)addAdventures:(NSOrderedSet *)values;
- (void)removeAdventures:(NSOrderedSet *)values;
@end
