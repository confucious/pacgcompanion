//
//  PACCampaign.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PACCharacter;

@interface PACCampaign : NSManagedObject

@property (nonatomic) NSTimeInterval createdOn;
@property (nonatomic) NSTimeInterval modifiedOn;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * participants;
@property (nonatomic, retain) NSOrderedSet *characters;
@end

@interface PACCampaign (CoreDataGeneratedAccessors)

- (void)insertObject:(PACCharacter *)value inCharactersAtIndex:(NSUInteger)idx;
- (void)removeObjectFromCharactersAtIndex:(NSUInteger)idx;
- (void)insertCharacters:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeCharactersAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInCharactersAtIndex:(NSUInteger)idx withObject:(PACCharacter *)value;
- (void)replaceCharactersAtIndexes:(NSIndexSet *)indexes withCharacters:(NSArray *)values;
- (void)addCharactersObject:(PACCharacter *)value;
- (void)removeCharactersObject:(PACCharacter *)value;
- (void)addCharacters:(NSOrderedSet *)values;
- (void)removeCharacters:(NSOrderedSet *)values;
@end
