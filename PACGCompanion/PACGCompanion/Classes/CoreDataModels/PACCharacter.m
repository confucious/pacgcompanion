//
//  PACCharacter.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

#import "PACCharacter.h"
#import "PACAdventurePath.h"
#import "PACCampaign.h"
#import "PACCard.h"
#import "PACCharacterProperty.h"
#import "PACRoleCard.h"


@implementation PACCharacter

@dynamic createdBy;
@dynamic createdOn;
@dynamic favoredCardType;
@dynamic flavorText;
@dynamic gender;
@dynamic job;
@dynamic name;
@dynamic notes;
@dynamic race;
@dynamic roleAdded;
@dynamic adventurePaths;
@dynamic campaign;
@dynamic cards;
@dynamic properties;
@dynamic roleCards;

@end
