//
//  PACAdventurePath.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import "PACAdventurePath.h"
#import "PACCharacter.h"


@implementation PACAdventurePath

@dynamic name;
@dynamic adventures;
@dynamic character;

@end
