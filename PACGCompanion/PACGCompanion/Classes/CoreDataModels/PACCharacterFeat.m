//
//  PACCharacterFeat.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/13/13.
//
//

#import "PACCharacterFeat.h"
#import "PACCharacterProperty.h"


@implementation PACCharacterFeat

@dynamic active;
@dynamic configId;
@dynamic defaultValue;
@dynamic value;
@dynamic property;

@end
