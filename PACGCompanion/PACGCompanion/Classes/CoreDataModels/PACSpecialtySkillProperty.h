//
//  PACSpecialtySkillProperty.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "PACCharacterProperty.h"

@class PACSkillProperty;

@interface PACSpecialtySkillProperty : PACCharacterProperty

@property (nonatomic, retain) PACSkillProperty *baseSkill;

@end
