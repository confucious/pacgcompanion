//
//  PACCharacter.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PACAdventurePath, PACCampaign, PACCard, PACCharacterProperty, PACRoleCard;

@interface PACCharacter : NSManagedObject

@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic) NSTimeInterval createdOn;
@property (nonatomic, retain) NSString * favoredCardType;
@property (nonatomic, retain) NSString * flavorText;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSString * job;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * race;
@property (nonatomic) BOOL roleAdded;
@property (nonatomic, retain) NSOrderedSet *adventurePaths;
@property (nonatomic, retain) PACCampaign *campaign;
@property (nonatomic, retain) NSSet *cards;
@property (nonatomic, retain) NSOrderedSet *properties;
@property (nonatomic, retain) NSOrderedSet *roleCards;
@end

@interface PACCharacter (CoreDataGeneratedAccessors)

- (void)insertObject:(PACAdventurePath *)value inAdventurePathsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromAdventurePathsAtIndex:(NSUInteger)idx;
- (void)insertAdventurePaths:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeAdventurePathsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInAdventurePathsAtIndex:(NSUInteger)idx withObject:(PACAdventurePath *)value;
- (void)replaceAdventurePathsAtIndexes:(NSIndexSet *)indexes withAdventurePaths:(NSArray *)values;
- (void)addAdventurePathsObject:(PACAdventurePath *)value;
- (void)removeAdventurePathsObject:(PACAdventurePath *)value;
- (void)addAdventurePaths:(NSOrderedSet *)values;
- (void)removeAdventurePaths:(NSOrderedSet *)values;
- (void)addCardsObject:(PACCard *)value;
- (void)removeCardsObject:(PACCard *)value;
- (void)addCards:(NSSet *)values;
- (void)removeCards:(NSSet *)values;

- (void)insertObject:(PACCharacterProperty *)value inPropertiesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromPropertiesAtIndex:(NSUInteger)idx;
- (void)insertProperties:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removePropertiesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInPropertiesAtIndex:(NSUInteger)idx withObject:(PACCharacterProperty *)value;
- (void)replacePropertiesAtIndexes:(NSIndexSet *)indexes withProperties:(NSArray *)values;
- (void)addPropertiesObject:(PACCharacterProperty *)value;
- (void)removePropertiesObject:(PACCharacterProperty *)value;
- (void)addProperties:(NSOrderedSet *)values;
- (void)removeProperties:(NSOrderedSet *)values;
- (void)insertObject:(PACRoleCard *)value inRoleCardsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRoleCardsAtIndex:(NSUInteger)idx;
- (void)insertRoleCards:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeRoleCardsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInRoleCardsAtIndex:(NSUInteger)idx withObject:(PACRoleCard *)value;
- (void)replaceRoleCardsAtIndexes:(NSIndexSet *)indexes withRoleCards:(NSArray *)values;
- (void)addRoleCardsObject:(PACRoleCard *)value;
- (void)removeRoleCardsObject:(PACRoleCard *)value;
- (void)addRoleCards:(NSOrderedSet *)values;
- (void)removeRoleCards:(NSOrderedSet *)values;
@end
