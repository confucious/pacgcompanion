//
//  PACRoleCard.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

#import "PACRoleCard.h"
#import "PACCharacter.h"


@implementation PACRoleCard

@dynamic name;
@dynamic data;
@dynamic character;

@end
