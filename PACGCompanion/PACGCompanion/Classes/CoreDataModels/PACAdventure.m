//
//  PACAdventure.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import "PACAdventure.h"
#import "PACAdventurePath.h"


@implementation PACAdventure

@dynamic name;
@dynamic adventurePath;
@dynamic scenarios;

@end
