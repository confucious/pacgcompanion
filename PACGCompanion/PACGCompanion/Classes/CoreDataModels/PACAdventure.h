//
//  PACAdventure.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PACAdventurePath;

@interface PACAdventure : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) PACAdventurePath *adventurePath;
@property (nonatomic, retain) NSOrderedSet *scenarios;
@end

@interface PACAdventure (CoreDataGeneratedAccessors)

- (void)insertObject:(NSManagedObject *)value inScenariosAtIndex:(NSUInteger)idx;
- (void)removeObjectFromScenariosAtIndex:(NSUInteger)idx;
- (void)insertScenarios:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeScenariosAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInScenariosAtIndex:(NSUInteger)idx withObject:(NSManagedObject *)value;
- (void)replaceScenariosAtIndexes:(NSIndexSet *)indexes withScenarios:(NSArray *)values;
- (void)addScenariosObject:(NSManagedObject *)value;
- (void)removeScenariosObject:(NSManagedObject *)value;
- (void)addScenarios:(NSOrderedSet *)values;
- (void)removeScenarios:(NSOrderedSet *)values;
@end
