//
//  PACRoleCard.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PACCharacter;

@interface PACRoleCard : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSData * data;
@property (nonatomic, retain) PACCharacter *character;

@end
