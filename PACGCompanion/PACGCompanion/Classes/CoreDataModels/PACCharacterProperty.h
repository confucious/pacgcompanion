//
//  PACCharacterProperty.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/13/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PACCharacter, PACCharacterFeat;

@interface PACCharacterProperty : NSManagedObject

@property (nonatomic, retain) NSString * configId;
@property (nonatomic, retain) NSString * effect;
@property (nonatomic, retain) NSString * name;
@property (nonatomic) BOOL optional;
@property (nonatomic) BOOL active;
@property (nonatomic, retain) PACCharacter *character;
@property (nonatomic, retain) NSOrderedSet *feats;
@end

@interface PACCharacterProperty (CoreDataGeneratedAccessors)

- (void)insertObject:(PACCharacterFeat *)value inFeatsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromFeatsAtIndex:(NSUInteger)idx;
- (void)insertFeats:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeFeatsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInFeatsAtIndex:(NSUInteger)idx withObject:(PACCharacterFeat *)value;
- (void)replaceFeatsAtIndexes:(NSIndexSet *)indexes withFeats:(NSArray *)values;
- (void)addFeatsObject:(PACCharacterFeat *)value;
- (void)removeFeatsObject:(PACCharacterFeat *)value;
- (void)addFeats:(NSOrderedSet *)values;
- (void)removeFeats:(NSOrderedSet *)values;
@end
