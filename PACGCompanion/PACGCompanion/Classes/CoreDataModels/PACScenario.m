//
//  PACScenario.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import "PACScenario.h"
#import "PACAdventure.h"


@implementation PACScenario

@dynamic completed;
@dynamic name;
@dynamic adventure;

@end
