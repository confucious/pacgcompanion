//
//  PACCard.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import "PACCard.h"
#import "PACCharacter.h"


@implementation PACCard

@dynamic name;
@dynamic removedFromGame;
@dynamic set;
@dynamic owner;

@end
