//
//  PACScenario.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PACAdventure;

@interface PACScenario : NSManagedObject

@property (nonatomic) BOOL completed;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) PACAdventure *adventure;

@end
