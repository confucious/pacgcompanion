//
//  PACSkillProperty.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "PACCharacterProperty.h"


@interface PACSkillProperty : PACCharacterProperty

@property (nonatomic, retain) NSOrderedSet *specialSkills;
@end

@interface PACSkillProperty (CoreDataGeneratedAccessors)

- (void)insertObject:(NSManagedObject *)value inSpecialSkillsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromSpecialSkillsAtIndex:(NSUInteger)idx;
- (void)insertSpecialSkills:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeSpecialSkillsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInSpecialSkillsAtIndex:(NSUInteger)idx withObject:(NSManagedObject *)value;
- (void)replaceSpecialSkillsAtIndexes:(NSIndexSet *)indexes withSpecialSkills:(NSArray *)values;
- (void)addSpecialSkillsObject:(NSManagedObject *)value;
- (void)removeSpecialSkillsObject:(NSManagedObject *)value;
- (void)addSpecialSkills:(NSOrderedSet *)values;
- (void)removeSpecialSkills:(NSOrderedSet *)values;
@end
