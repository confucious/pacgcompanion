//
//  PACModels.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/13/13.
//
//

#import <Foundation/Foundation.h>
#import "PACAdventure.h"
#import "PACAdventurePath.h"
#import "PACAllyCard.h"
#import "PACArmorCard.h"
#import "PACBarrierCard.h"
#import "PACBlessingCard.h"
#import "PACCampaign.h"
#import "PACCard.h"
#import "PACCardListProperty.h"
#import "PACCharacter.h"
#import "PACCharacterFeat.h"
#import "PACCharacterProperty.h"
#import "PACItemCard.h"
#import "PACLootCard.h"
#import "PACMonsterCard.h"
#import "PACPowerProperty.h"
#import "PACRoleCard.h"
#import "PACScenario.h"
#import "PACSkillProperty.h"
#import "PACSpecialtySkillProperty.h"
#import "PACSpellCard.h"
#import "PACWeaponCard.h"

#define COREDATA_INIT_INTERFACE(type) \
@interface type (CoreDataInit) \
+ (type *)instanceInManagedObjectContext:(NSManagedObjectContext *)context;\
@end

COREDATA_INIT_INTERFACE(PACAdventure)
COREDATA_INIT_INTERFACE(PACAdventurePath)
COREDATA_INIT_INTERFACE(PACAllyCard)
COREDATA_INIT_INTERFACE(PACArmorCard)
COREDATA_INIT_INTERFACE(PACBarrierCard)
COREDATA_INIT_INTERFACE(PACBlessingCard)
COREDATA_INIT_INTERFACE(PACCampaign)
COREDATA_INIT_INTERFACE(PACCardListProperty)
COREDATA_INIT_INTERFACE(PACCharacter)
COREDATA_INIT_INTERFACE(PACCharacterFeat)
COREDATA_INIT_INTERFACE(PACItemCard)
COREDATA_INIT_INTERFACE(PACLootCard)
COREDATA_INIT_INTERFACE(PACMonsterCard)
COREDATA_INIT_INTERFACE(PACPowerProperty)
COREDATA_INIT_INTERFACE(PACRoleCard)
COREDATA_INIT_INTERFACE(PACScenario)
COREDATA_INIT_INTERFACE(PACSkillProperty)
COREDATA_INIT_INTERFACE(PACSpecialtySkillProperty)
COREDATA_INIT_INTERFACE(PACSpellCard)
COREDATA_INIT_INTERFACE(PACWeaponCard)
