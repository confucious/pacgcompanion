//
//  PACCampaign.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import "PACCampaign.h"
#import "PACCharacter.h"


@implementation PACCampaign

@dynamic createdOn;
@dynamic modifiedOn;
@dynamic notes;
@dynamic participants;
@dynamic characters;

@end
