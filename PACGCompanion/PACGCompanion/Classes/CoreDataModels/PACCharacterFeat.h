//
//  PACCharacterFeat.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/13/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PACCharacterProperty;

@interface PACCharacterFeat : NSManagedObject

@property (nonatomic) BOOL active;
@property (nonatomic, retain) NSString * configId;
@property (nonatomic) BOOL defaultValue;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) PACCharacterProperty *property;

@end
