//
//  PACStartView.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/19/13.
//
//

#import "PACStartView.h"

@implementation PACStartView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _resumeCampaignButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [_resumeCampaignButton setTitle:@"Resume Campaign" forState:UIControlStateNormal];
        _startCampaignButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [_startCampaignButton setTitle:@"Start Campaign" forState:UIControlStateNormal];
        [self addSubview:_resumeCampaignButton];
        [self addSubview:_startCampaignButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    CGFloat xOrigin = CGRectGetMidX(self.bounds) - 100.0;
    CGFloat yCenter = CGRectGetMidY(self.bounds);
    self.resumeCampaignButton.frame = CGRectMake(xOrigin, yCenter - 60.0, 200.0, 50.0);
    self.startCampaignButton.frame = CGRectMake(xOrigin, yCenter + 10.0, 200.0, 50.0);
}

@end
