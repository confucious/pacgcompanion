//
//  PACCharacterPropertyCellContent.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/27/13.
//
//

#import <UIKit/UIKit.h>
#import "PACCharacterProperty.h"
#import "PACCharacterFeat.h"

typedef enum {
    PACCharacterPropertyCellContentBasic,
    PACCharacterPropertyCellContentExtended,
    PACCharacterPropertyCellContentEdit
} PACCharacterPropertyCellContentMode;

@protocol PACCharacterPropertyCellContentDelegate;

@interface PACCharacterPropertyCellContent : UITextView

@property (nonatomic, assign) PACCharacterPropertyCellContentMode mode;
@property (nonatomic, weak) id <PACCharacterPropertyCellContentDelegate> actionDelegate;
@property (nonatomic, strong) PACCharacterProperty *property;
/// targetWidth will be used for multi-line word wrapping. Defaults to 320.0.
@property (nonatomic, assign) CGFloat targetWidth;

- (void)updateTextViewContent;

@end

@protocol PACCharacterPropertyCellContentDelegate

- (void)characterPropertyCellTappedOptionalCheckbox:(PACCharacterPropertyCellContent *)cell;
- (void)characterPropertyCell:(PACCharacterPropertyCellContent *)cell tappedFeat:(PACCharacterFeat *)feat;

@end
