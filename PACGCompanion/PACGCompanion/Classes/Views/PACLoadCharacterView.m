//
//  PACLoadCharacterView.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/19/13.
//
//

#import "PACLoadCharacterView.h"

@implementation PACLoadCharacterView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UITextField *playerName = [[UITextField alloc] initWithFrame:CGRectMake(0.0, 20.0, self.bounds.size.width, 44.0)];
        playerName.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.playerName = playerName;

        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 64.0, self.bounds.size.width, self.bounds.size.height - 64.0)];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.tableView = tableView;

        [self addSubview:playerName];
        [self addSubview:tableView];
    }
    return self;
}

@end
