//
//  PACStartView.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/19/13.
//
//

#import <UIKit/UIKit.h>

@interface PACStartView : UIView

@property (nonatomic, strong) UIButton *resumeCampaignButton;
@property (nonatomic, strong) UIButton *startCampaignButton;

@end
