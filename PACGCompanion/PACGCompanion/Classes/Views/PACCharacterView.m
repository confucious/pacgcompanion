//
//  PACCharacterView.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/25/13.
//
//

#import "PACCharacterView.h"
#import "PACColumnCollectionViewLayout.h"

@implementation PACCharacterView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _navBar = [[UINavigationBar alloc] init];
        _collectionViewLayout = [[PACColumnCollectionViewLayout alloc] init];
        _collectionViewLayout.minimumInteritemSpacing = 0.0;
        _collectionViewLayout.minimumLineSpacing = 1.0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_collectionViewLayout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_collectionView];
        [self addSubview:_navBar];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect bounds = self.bounds;
    CGFloat navBarHeight = 44.0;
    self.navBar.frame = CGRectMake(0.0, 0.0, bounds.size.width, navBarHeight);
    self.collectionView.frame = CGRectMake(0.0, 0.0, bounds.size.width, bounds.size.height);
    self.collectionView.contentInset = UIEdgeInsetsMake(navBarHeight, 0.0, 0.0, 0.0);
}

@end
