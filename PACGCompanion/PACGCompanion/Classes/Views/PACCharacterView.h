//
//  PACCharacterView.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/25/13.
//
//

#import <UIKit/UIKit.h>

@interface PACCharacterView : UIView

@property (nonatomic, strong) UINavigationBar *navBar;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *collectionViewLayout;

@end
