//
//  PACCharacterPropertyCellContent.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/27/13.
//
//

#import "PACCharacterPropertyCellContent.h"
#import "PACCharacterManager.h"

@interface PACCharacterPropertyCellContent ()

@end

@implementation PACCharacterPropertyCellContent

- (id)initWithFrame:(CGRect)frame textContainer:(NSTextContainer *)textContainer{
    self = [super initWithFrame:frame textContainer:textContainer];
    if (self) {
        self.scrollEnabled = NO;
        self.editable = NO;
        self.selectable = NO;
        self.textContainerInset = UIEdgeInsetsMake(1.0, 2.0, 1.0, 2.0);
        self.textContainer.lineFragmentPadding = 0.0;
        self.targetWidth = 320.0;
//        self.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.25];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}

- (void)setProperty:(PACCharacterProperty *)property {
    if (property != _property) {
        _property = property;
        [self updateTextViewContent];
    }
}

- (void)setMode:(PACCharacterPropertyCellContentMode)mode {
    if (mode != _mode) {
        _mode = mode;
        [self updateTextViewContent];
    }
}

- (void)updateTextViewContent {
    switch (self.mode) {
        case PACCharacterPropertyCellContentBasic: {
            [self updateTextViewForBasic];
        }
            break;

        case PACCharacterPropertyCellContentEdit: {
            [self updateTextViewForEdit];
        }
            break;

        default:
            break;
    }
}

- (void)updateTextViewForBasic {
    self.attributedText = [PACCharacterManager compactAttributedDescriptionOfProperty:self.property];
}

- (void)updateTextViewForEdit {
    self.attributedText = [PACCharacterManager expandedAttributedDescriptionOfProperty:self.property];
}

- (CGSize)sizeThatFits:(CGSize)size {
    NSTextContainer *textContainer = self.textContainer;
    UIEdgeInsets insets = self.textContainerInset;
    textContainer.widthTracksTextView = NO;
    textContainer.heightTracksTextView = NO;
    textContainer.size = CGSizeMake(self.targetWidth - insets.left - insets.right, CGFLOAT_MAX);
    NSRange glyphRange = NSMakeRange(0, [textContainer.layoutManager numberOfGlyphs]);
    CGRect bounds = [textContainer.layoutManager boundingRectForGlyphRange:glyphRange
                                                           inTextContainer:textContainer];
    bounds.size.width = self.targetWidth;
    bounds.size.height = ceilf(bounds.size.height) + insets.top + insets.bottom;
    //NSLog(@"%@, %f, %f", self.text, bounds.size.width, bounds.size.height);
    return bounds.size;
}

#pragma mark - Gesture Recognizers

- (void)handleTapGesture:(UITapGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint location = [recognizer locationInView:self];
        UIEdgeInsets insets = self.textContainerInset;
        location.x += insets.left;
        location.y += insets.top;
        NSInteger glyphIndex = [self.layoutManager glyphIndexForPoint:location inTextContainer:self.textContainer fractionOfDistanceThroughGlyph:nil];
        CGRect boundingRect = [self.layoutManager boundingRectForGlyphRange:NSMakeRange(glyphIndex, 1) inTextContainer:self.textContainer];
        if (!CGRectContainsPoint(boundingRect, location)) {
            NSLog(@"touch at %f,%f with index %d did not intersect a location", location.x, location.y, glyphIndex);
            return;
        }
        NSInteger characterIndex = [self.layoutManager characterIndexForGlyphAtIndex:glyphIndex];
        NSDictionary *attributes = [self.attributedText attributesAtIndex:characterIndex effectiveRange:nil];
        NSLog(@"attributes %@", attributes);
        if (attributes[@"PACOptionalChecked"]) {
            [self.actionDelegate characterPropertyCellTappedOptionalCheckbox:self];
        } else if (attributes[@"PACFeat"]) {
            [self.actionDelegate characterPropertyCell:self tappedFeat:attributes[@"PACFeat"]];
        }
    }
}

@end
