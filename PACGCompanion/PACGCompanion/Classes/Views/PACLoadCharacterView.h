//
//  PACLoadCharacterView.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/19/13.
//
//

#import <UIKit/UIKit.h>

@interface PACLoadCharacterView : UIView

@property (nonatomic, strong) UITextField *playerName;
@property (nonatomic, strong) UITableView *tableView;

@end
