//
//  PACColumnCollectionViewLayout.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 1/2/14.
//
//

#import <UIKit/UIKit.h>

@protocol PACColumnCollectionViewLayoutDelegate <UICollectionViewDelegateFlowLayout>
@optional
- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout numberOfColumnsForSectionAtIndex:(NSInteger)section;

@end

@interface PACColumnCollectionViewLayout : UICollectionViewFlowLayout

@property (nonatomic, assign) NSInteger numberOfColumns;
@property (nonatomic, assign) NSInteger tag;
/// Defaults to top alignment.
@property (nonatomic, assign) UIControlContentVerticalAlignment verticalCellAlignment;
@property (nonatomic, assign) CGFloat minimumRowHeight;

@end
