//
//  PACColumnCollectionViewDividerView.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 1/4/14.
//
//

#import "PACColumnCollectionViewDividerView.h"

@implementation PACColumnCollectionViewDividerView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor blackColor];
    }
    return self;
}

@end
