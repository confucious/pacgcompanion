//
//  PACSectionRange.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/27/13.
//
//

#import "PACSectionRange.h"

@implementation PACSectionRange

- (id)initWithLocation:(NSUInteger)location andLength:(NSUInteger)length {
    self = [super init];
    if (self) {
        _range.location = location;
        _range.length = length;
    }
    return self;
}

- (NSUInteger)location {
    return _range.location;
}

- (void)setLocation:(NSUInteger)location {
    _range.location = location;
}

- (NSUInteger)length {
    return _range.length;
}

- (void)setLength:(NSUInteger)length {
    _range.length = length;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%d.%d", _range.location, _range.length];
}

@end
