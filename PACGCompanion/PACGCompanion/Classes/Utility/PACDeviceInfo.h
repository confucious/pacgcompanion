//
//  PACDeviceInfo.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/23/13.
//
//

#import <Foundation/Foundation.h>

extern BOOL iPad;

@interface PACDeviceInfo : NSObject

+ (void)setGlobalVariables;

@end
