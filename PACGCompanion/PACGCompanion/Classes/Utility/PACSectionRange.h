//
//  PACSectionRange.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/27/13.
//
//

#import <Foundation/Foundation.h>

@interface PACSectionRange : NSObject

@property (nonatomic, assign) NSRange range;
@property (nonatomic, assign) NSUInteger location;
@property (nonatomic, assign) NSUInteger length;

- (id)initWithLocation:(NSUInteger)location andLength:(NSUInteger)length;

@end
