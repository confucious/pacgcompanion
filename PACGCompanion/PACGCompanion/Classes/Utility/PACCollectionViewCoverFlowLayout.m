//
//  PACCollectionViewCoverFlowLayout.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/23/13.
//
//

#import "PACCollectionViewCoverFlowLayout.h"

@implementation PACCollectionViewCoverFlowLayout

- (id)init {
    self = [super init];
    if (self) {
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.minimumLineSpacing = 20.0;
    }
    return self;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

-(NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    // Extend rect to get additional elements
    CGRect itemRect = CGRectInset(rect, -(320.0+20.0) * 3, 0.0);
    NSArray* array = [super layoutAttributesForElementsInRect:itemRect];
    CGRect visibleRect;
    visibleRect.origin = self.collectionView.contentOffset;
    visibleRect.size = self.collectionView.bounds.size;
    NSLog(@"%f %f %f", visibleRect.origin.x, visibleRect.size.width, CGRectGetMidX(visibleRect));

    CGFloat activeDistance = self.itemSize.width;

    CGFloat midX = CGRectGetMidX(visibleRect);
    for (UICollectionViewLayoutAttributes* attributes in array) {
        CGFloat distance = midX - attributes.center.x;
        CGFloat normalizedDistance = distance / activeDistance;
        CGFloat absNormal = ABS(normalizedDistance);
        if (absNormal < 2.0) {
            NSLog(@"distance %f normalizedDistance %f", distance, normalizedDistance);
        }
        attributes.zIndex = -abs(distance * 10);
        CGFloat scale = MAX(1.0-absNormal, 0.8);
//        attributes.transform3D = CATransform3DMakeTranslation(0.0, 0.0, -absNormal * 10.0);
//        attributes.transform3D = CATransform3DMakeScale(scale, scale, 1.0);
//        if (absNormal > 0.5) {
        CGFloat scrollFactor = powf(MIN(absNormal, 1.0), 2.0) * 2.0 + 1.0;
//        CGFloat offset = (attributes.center.x - midX) / scrollFactor;
//        CGFloat offset = normalizedDistance * 80.0;
//            attributes.center = CGPointMake(offset + attributes.center.x, attributes.center.y);
//        NSLog(@"%@ %f %f", attributes.indexPath, attributes.center.x, offset);
//        }
//        attributes.transform3D = CATransform3DMakeRotation(normalizedDistance * M_PI / 2.5, 0.0, 1.0, 0.0);
//        attributes.transform3D = CATransform3DTranslate(attributes.transform3D, 0.0, 0.0, -absNormal * 50.0);
        attributes.transform3D = CATransform3DMakeTranslation(0.0, 0.0, -100.0);
//        NSLog(@"%@", attributes);
    }

    return array;
}

- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity {
    CGFloat offsetAdjustment = MAXFLOAT;
    CGFloat horizontalCenter = proposedContentOffset.x + (CGRectGetWidth(self.collectionView.bounds) / 2.0);

    CGRect targetRect = CGRectMake(proposedContentOffset.x, 0.0, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
    NSArray* array = [super layoutAttributesForElementsInRect:targetRect];

    for (UICollectionViewLayoutAttributes* layoutAttributes in array) {
        CGFloat itemHorizontalCenter = layoutAttributes.center.x;
        if (ABS(itemHorizontalCenter - horizontalCenter) < ABS(offsetAdjustment)) {
            offsetAdjustment = itemHorizontalCenter - horizontalCenter;
        }
    }
    CGPoint ret = CGPointMake(proposedContentOffset.x + offsetAdjustment, proposedContentOffset.y);
    return ret;
}

@end
