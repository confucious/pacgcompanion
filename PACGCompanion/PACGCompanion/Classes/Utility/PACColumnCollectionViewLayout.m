//
//  PACColumnCollectionViewLayout.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 1/2/14.
//
//

#import "PACColumnCollectionViewLayout.h"
#import "PACColumnCollectionViewDividerView.h"

@interface PACColumnCollectionViewLayout ()

/// NSInteger count of the number of columns in each section.
@property (nonatomic, strong) NSMutableDictionary *columnCountCache;
/// UICollectionViewLayoutAttributes by indexPath
@property (nonatomic, strong) NSMutableDictionary *headerAttributes;
@property (nonatomic, strong) NSMutableDictionary *elementAttributes;
@property (nonatomic, strong) NSMutableDictionary *decorationAttributes;
@property (nonatomic, assign) CGFloat overallHeight;

@end

@implementation PACColumnCollectionViewLayout

- (id)init {
    self = [super init];
    if (self) {
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
        self.minimumLineSpacing = 1.0;
        _columnCountCache = [[NSMutableDictionary alloc] init];
        _headerAttributes = [[NSMutableDictionary alloc] init];
        _elementAttributes = [[NSMutableDictionary alloc] init];
        _decorationAttributes = [[NSMutableDictionary alloc] init];
        _numberOfColumns = 1;
        _verticalCellAlignment = UIControlContentVerticalAlignmentTop;
    }
    return self;
}

- (NSInteger)numberOfColumnsInSection:(NSInteger)sectionIndex {
    NSNumber *sectionKey = @(sectionIndex);
    NSNumber *columnCount = self.columnCountCache[sectionKey];
    if (columnCount == nil) {
        UICollectionView <PACColumnCollectionViewLayoutDelegate> *delegate = (id) self.collectionView.delegate;
        if ([delegate respondsToSelector:@selector(collectionView:layout:numberOfColumnsForSectionAtIndex:)]) {
            columnCount = @([delegate collectionView:self.collectionView layout:self numberOfColumnsForSectionAtIndex:sectionIndex]);
        } else {
            columnCount = @(self.numberOfColumns);
        }
        self.columnCountCache[sectionKey] = columnCount;
    }
    return [columnCount integerValue];
}

- (void)invalidateLayout {
    [super invalidateLayout];
    [self.columnCountCache removeAllObjects];
    [self.elementAttributes removeAllObjects];
    self.overallHeight = -1.0;
}

- (CGSize)collectionViewContentSize {
    if (self.overallHeight < 0.0) {
        [self prepareLayout];
    }
    return CGSizeMake(self.collectionView.bounds.size.width, self.overallHeight);
}

- (void)prepareLayout {
    UICollectionView *collectionView = self.collectionView;
    CGFloat originY = 0.0;
    NSInteger numSections = [collectionView numberOfSections];
    for (NSInteger sectionIndex = 0; sectionIndex < numSections; sectionIndex++) {
        originY = [self layoutHeaderForSection:sectionIndex startingAtY:originY];
        originY = [self layoutItemsInSection:sectionIndex startingAtY:originY];
    }
    self.overallHeight = originY;
}

- (CGFloat)layoutHeaderForSection:(NSInteger)sectionIndex startingAtY:(CGFloat)originY {
    UICollectionView *collectionView = self.collectionView;
    UICollectionView <PACColumnCollectionViewLayoutDelegate> *delegate = (id) collectionView.delegate;
    CGSize size = self.headerReferenceSize;
    if ([delegate respondsToSelector:@selector(collectionView:layout:referenceSizeForHeaderInSection:)]) {
        size = [delegate collectionView:collectionView layout:self referenceSizeForHeaderInSection:sectionIndex];
    }
    if (size.width != 0.0 && size.height != 0.0) {
        NSIndexPath *headerIndexPath = [NSIndexPath indexPathForItem:0 inSection:sectionIndex];
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:@"header"
                                                                                                                      withIndexPath:headerIndexPath];
        attributes.frame = CGRectMake(0.0, originY, collectionView.bounds.size.width, size.height);
        self.headerAttributes[headerIndexPath] = attributes;
    }
    return originY + size.height;
}

- (CGFloat)layoutItemsInSection:(NSInteger)sectionIndex startingAtY:(CGFloat)originY {
    UICollectionView *collectionView = self.collectionView;
    UICollectionView <PACColumnCollectionViewLayoutDelegate> *delegate = (id) collectionView.delegate;
    CGSize defaultItemSize = self.itemSize;
    CGFloat lineSpacing = self.minimumLineSpacing;
    NSInteger columnCount = [self numberOfColumnsInSection:sectionIndex];
    if (columnCount < 1) {
        columnCount = 1;
    }
    NSInteger numItems = [collectionView numberOfItemsInSection:sectionIndex];
    NSInteger itemsPerColumn = (numItems + columnCount - 1) / columnCount;
    CGFloat subOriginY = originY;
    CGFloat columnWidth = floorf(collectionView.bounds.size.width / (float)columnCount);
    NSMutableArray *rowItemAttributes = [[NSMutableArray alloc] initWithCapacity:columnCount];
    NSArray *unusedLinePaddingAttributes = nil;
    for (NSInteger index = 0; index < numItems; index++) {
        NSInteger row = index / columnCount;
        NSInteger column = index % columnCount;
        NSInteger itemIndex = row + itemsPerColumn * column;

        // Size all items row by row finding the maxHeight for each one.
        CGSize size = defaultItemSize;
        NSIndexPath *itemIndexPath = [NSIndexPath indexPathForItem:itemIndex inSection:sectionIndex];
        if ([delegate respondsToSelector:@selector(collectionView:layout:sizeForItemAtIndexPath:)]) {
            size = [delegate collectionView:collectionView layout:self sizeForItemAtIndexPath:itemIndexPath];
        } else {
            size = self.itemSize;
        }
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:itemIndexPath];
        [rowItemAttributes addObject:attributes];
        attributes.frame = CGRectMake(column * columnWidth, originY, size.width, size.height);
        //NSLog(@"%@", attributes);
        self.elementAttributes[itemIndexPath] = attributes;
        subOriginY = MAX(subOriginY, originY + size.height);
        if (column == (columnCount - 1) || (index - 1) == numItems) {
            CGFloat rowHeight = subOriginY - originY;
            if (rowHeight < self.minimumRowHeight) {
                rowHeight = self.minimumRowHeight;
                subOriginY = originY + rowHeight;
            }
            originY = subOriginY;
            if (rowHeight > 0.0) {
                [rowItemAttributes enumerateObjectsUsingBlock:^(UICollectionViewLayoutAttributes *itemAttributes, NSUInteger column, BOOL *stop) {
                    switch (self.verticalCellAlignment) {
                        case UIControlContentVerticalAlignmentCenter: {
                            CGRect frame = itemAttributes.frame;
                            frame.origin.y += roundf((rowHeight - frame.size.height) / 2.0);
                            itemAttributes.frame = frame;
                        }
                            break;

                        case UIControlContentVerticalAlignmentBottom: {
                            CGRect frame = itemAttributes.frame;
                            frame.origin.y += rowHeight - frame.size.height;
                            itemAttributes.frame = frame;
                        }
                            break;

                        case UIControlContentVerticalAlignmentFill: {
                            CGRect frame = itemAttributes.frame;
                            frame.size.height = rowHeight;
                            itemAttributes.frame = frame;
                        }
                            break;

                        case UIControlContentVerticalAlignmentTop:
                        default:
                            // Do nothing.
                            break;
                    }
                    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:@"divider"
                                                                                                                                  withIndexPath:itemAttributes.indexPath];
                    attributes.frame = CGRectMake(column * columnWidth, originY, size.width, 1.0);
                    self.decorationAttributes[itemAttributes.indexPath] = attributes;
                }];
                unusedLinePaddingAttributes = [rowItemAttributes copy];
                originY += lineSpacing;
            }
            [rowItemAttributes removeAllObjects];
        }
    }
    if (unusedLinePaddingAttributes) {
        [unusedLinePaddingAttributes enumerateObjectsUsingBlock:^(UICollectionViewLayoutAttributes *attributes, NSUInteger idx, BOOL *stop) {
            [self.decorationAttributes removeObjectForKey:attributes.indexPath];
        }];
        originY -= lineSpacing;
    }
    return originY;
}

-(NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray *visibleItems = [[NSMutableArray alloc] init];
    [self.headerAttributes enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath, UICollectionViewLayoutAttributes *attributes, BOOL *stop) {
        if (CGRectIntersectsRect(attributes.frame, rect)) {
            [visibleItems addObject:attributes];
        }
    }];
    [self.elementAttributes enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath, UICollectionViewLayoutAttributes *attributes, BOOL *stop) {
        if (CGRectIntersectsRect(attributes.frame, rect)) {
            [visibleItems addObject:attributes];
        }
    }];
    [self.decorationAttributes enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath, UICollectionViewLayoutAttributes *attributes, BOOL *stop) {
        if (CGRectIntersectsRect(attributes.frame, rect)) {
            [visibleItems addObject:attributes];
        }
    }];
    //NSLog(@"layout %d\n%@", self.tag, visibleItems);
    return visibleItems;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.elementAttributes[indexPath];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:@"header"]) {
        return self.headerAttributes[[NSIndexPath indexPathForItem:0 inSection:indexPath.section]];
    } else if ([kind isEqualToString:@"divider"]) {
        return self.decorationAttributes[indexPath];
    }
    return nil;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return NO;
}

@end
