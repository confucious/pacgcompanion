//
//  PACDeviceInfo.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/23/13.
//
//

#import "PACDeviceInfo.h"

BOOL iPad;

@implementation PACDeviceInfo

+ (void)setGlobalVariables {
    iPad = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad;
}

@end
