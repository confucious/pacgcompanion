//
//  PACCharacterManagerTests.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/13/13.
//
//

#import <XCTest/XCTest.h>
#import "PACCharacterManager.h"
#import "PACCharacterProperty.h"
#import "PACCharacterFeat.h"

@interface PACCharacterManager ()

@property (nonatomic, strong) NSRegularExpression *featRegex;
@property (nonatomic, strong) NSRegularExpression *whitespaceCleaningRegex;

@end

@interface PACCharacterManagerTests : XCTestCase

@property (nonatomic, strong) NSArray *testFiles;

@end

@implementation PACCharacterManagerTests

- (void)setUp {
    [super setUp];
    self.testFiles = [PACCharacterManager availableCharacters];
}

- (void)tearDown {
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (NSManagedObjectContext *) managedObjectContext {

    NSManagedObjectContext *context;
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        context = [[NSManagedObjectContext alloc] init];
        [context setPersistentStoreCoordinator:coordinator];
    }
    return context;
}

- (NSManagedObjectModel *)managedObjectModel {
    return [NSManagedObjectModel mergedModelFromBundles:nil];
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {

	NSError *error = nil;
    NSPersistentStoreCoordinator *persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSInMemoryStoreType
                                                  configuration:nil URL:nil
                                                        options:options error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
    }

    return persistentStoreCoordinator;
}

- (void)testStartupDataPopulation {
    NSManagedObjectContext *context = [self managedObjectContext];
    [self.testFiles enumerateObjectsUsingBlock:^(NSString *filename, NSUInteger idx, BOOL *stop) {
        PACCharacterManager *manager = [[PACCharacterManager alloc] initWithManagedObjectContext:context];
        [manager populateBasicCharacterFromJSONFile:filename];
        XCTAssertEqual([manager.character.roleCards count], (NSUInteger) 2, @"Didn't see enough role cards");
        NSLog(@"%@", [manager compactDescriptionOfCharacter]);
        NSLog(@"%@", [manager expandedDescriptionOfCharacter]);
    }];
}

- (void)testRoleApplication {
    NSManagedObjectContext *context = [self managedObjectContext];
    [self.testFiles enumerateObjectsUsingBlock:^(NSString *filename, NSUInteger idx, BOOL *stop) {
        PACCharacterManager *manager = [[PACCharacterManager alloc] initWithManagedObjectContext:context];
        [manager populateBasicCharacterFromJSONFile:filename];
        NSString *roleName = manager.roleCardNames[0];
        [manager applyRoleCardWithName:roleName];
        XCTAssertEqualObjects(manager.character.job, roleName, @"role name0 mismatch");
        NSLog(@"%@", [manager compactDescriptionOfCharacter]);
        NSLog(@"%@", [manager expandedDescriptionOfCharacter]);
    }];
    [self.testFiles enumerateObjectsUsingBlock:^(NSString *filename, NSUInteger idx, BOOL *stop) {
        PACCharacterManager *manager = [[PACCharacterManager alloc] initWithManagedObjectContext:context];
        [manager populateBasicCharacterFromJSONFile:filename];
        NSString *roleName = manager.roleCardNames[1];
        [manager applyRoleCardWithName:roleName];
        XCTAssertEqualObjects(manager.character.job, roleName, @"role name1 mismatch");
        NSLog(@"%@", [manager compactDescriptionOfCharacter]);
        NSLog(@"%@", [manager expandedDescriptionOfCharacter]);
    }];
}

- (void)testApplyingFeats {
    NSManagedObjectContext *context = [self managedObjectContext];
    [self.testFiles enumerateObjectsUsingBlock:^(NSString *filename, NSUInteger idx, BOOL *stop) {
        PACCharacterManager *manager = [[PACCharacterManager alloc] initWithManagedObjectContext:context];
        [manager populateBasicCharacterFromJSONFile:filename];
        [manager applyRoleCardWithName:manager.roleCardNames[0]];
        [manager.character.properties enumerateObjectsUsingBlock:^(PACCharacterProperty *property, NSUInteger idx, BOOL *stop) {
            if (property.optional) {
                property.active = YES;
            }
            [property.feats enumerateObjectsUsingBlock:^(PACCharacterFeat *feat, NSUInteger idx, BOOL *stop) {
                if (!feat.active) {
                    feat.active = YES;
                }
            }];
        }];
        NSLog(@"%@", [manager compactDescriptionOfCharacter]);
        NSLog(@"%@", [manager expandedDescriptionOfCharacter]);
    }];
    [self.testFiles enumerateObjectsUsingBlock:^(NSString *filename, NSUInteger idx, BOOL *stop) {
        PACCharacterManager *manager = [[PACCharacterManager alloc] initWithManagedObjectContext:context];
        [manager populateBasicCharacterFromJSONFile:filename];
        [manager applyRoleCardWithName:manager.roleCardNames[1]];
        [manager.character.properties enumerateObjectsUsingBlock:^(PACCharacterProperty *property, NSUInteger idx, BOOL *stop) {
            if (property.optional) {
                property.active = YES;
            }
            [property.feats enumerateObjectsUsingBlock:^(PACCharacterFeat *feat, NSUInteger idx, BOOL *stop) {
                if (!feat.active) {
                    feat.active = YES;
                }
            }];
        }];
        NSLog(@"%@", [manager compactDescriptionOfCharacter]);
        NSLog(@"%@", [manager expandedDescriptionOfCharacter]);
    }];
}

- (void)testRegex1 {
    PACCharacterManager *manager = [[PACCharacterManager alloc] initWithManagedObjectContext:nil];
    NSRegularExpression *featRegex;
    XCTAssertNoThrow(featRegex = manager.featRegex, @"Getting regex threw exception.");

    NSString *testString = @"A %feat% something";
    NSArray *matches = [featRegex matchesInString:testString options:0 range:NSMakeRange(0, [testString length])];
    XCTAssertEqual([matches count], (NSUInteger)1, @"number of matches for %@ was wrong", testString);
    [matches enumerateObjectsUsingBlock:^(NSTextCheckingResult *match, NSUInteger idx, BOOL *stop) {
        XCTAssertEqual(match.numberOfRanges, (NSUInteger)3, @"number of submatches for %@ was wrong", testString);
        XCTAssertEqual([match rangeAtIndex:0], NSMakeRange(2,6), @"Overall match size for %@ was wrong", testString);
        XCTAssertEqual([match rangeAtIndex:1], NSMakeRange(3,0), @"DefaultValue match %@ was wrong", testString);
        XCTAssertEqual([match rangeAtIndex:2], NSMakeRange(3,4), @"ConfigId match %@ was wrong", testString);
    }];
}

- (void)testRegex2 {
    PACCharacterManager *manager = [[PACCharacterManager alloc] initWithManagedObjectContext:nil];
    NSRegularExpression *featRegex;
    XCTAssertNoThrow(featRegex = manager.featRegex, @"Getting regex threw exception.");

    NSString *testString = @"A %2:feat% something %3:feat2%";
    NSArray *matches = [featRegex matchesInString:testString options:0 range:NSMakeRange(0, [testString length])];
    XCTAssertEqual([matches count], (NSUInteger)2, @"number of matches for %@ was wrong", testString);

    NSTextCheckingResult *match1 = matches[0];
    XCTAssertEqual(match1.numberOfRanges, (NSUInteger)3, @"1 number of submatches for %@ was wrong", testString);
    XCTAssertEqual([match1 rangeAtIndex:0], NSMakeRange(2,8), @"1 Overall match size for %@ was wrong", testString);
    XCTAssertEqual([match1 rangeAtIndex:1], NSMakeRange(3,1), @"1 DefaultValue match %@ was wrong", testString);
    XCTAssertEqual([match1 rangeAtIndex:2], NSMakeRange(5,4), @"1 ConfigId match %@ was wrong", testString);
    XCTAssertEqualObjects([testString substringWithRange:[match1 rangeAtIndex:1]], @"2", @"1 defaultValue %@ was wrong", testString);
    XCTAssertEqualObjects([testString substringWithRange:[match1 rangeAtIndex:2]], @"feat", @"1 configId %@ was wrong", testString);

    NSTextCheckingResult *match2 = matches[1];
    XCTAssertEqual(match2.numberOfRanges, (NSUInteger)3, @"2 number of submatches for %@ was wrong", testString);
    XCTAssertEqual([match2 rangeAtIndex:0], NSMakeRange(21,9), @"2 Overall match size for %@ was wrong", testString);
    XCTAssertEqual([match2 rangeAtIndex:1], NSMakeRange(22,1), @"2 DefaultValue match %@ was wrong", testString);
    XCTAssertEqual([match2 rangeAtIndex:2], NSMakeRange(24,5), @"2 ConfigId match %@ was wrong", testString);
    XCTAssertEqualObjects([testString substringWithRange:[match2 rangeAtIndex:1]], @"3", @"2 defaultValue %@ was wrong", testString);
    XCTAssertEqualObjects([testString substringWithRange:[match2 rangeAtIndex:2]], @"feat2", @"2 configId %@ was wrong", testString);
}

- (void)testRegexWhiteSpace {
    PACCharacterManager *manager = [[PACCharacterManager alloc] initWithManagedObjectContext:nil];
    NSRegularExpression *regex;
    XCTAssertNoThrow(regex = manager.whitespaceCleaningRegex, @"Getting regex threw exception.");

    NSString *testString = @"A  something , needs cleaning .";
    NSString *result = [regex stringByReplacingMatchesInString:testString options:0 range:NSMakeRange(0, [testString length]) withTemplate:@"$2"];
    XCTAssertEqualObjects(result, @"A something, needs cleaning.", @"whitespace cleaning");
}

@end
