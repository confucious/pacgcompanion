//
//  PACCharacterManager.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import "PACCharacterManager.h"
#import "PACModels.h"

@interface PACCharacterManager ()

@property (nonatomic, strong) NSRegularExpression *featRegex;
@property (nonatomic, strong) NSRegularExpression *whitespaceCleaningRegex;
// Map of NSString * name to NSArray * of powers.
@property (nonatomic, strong) NSDictionary *roleCards;

@end

@implementation PACCharacterManager

+ (NSArray *)availableCharacters {
    return @[@"Ezren", @"Harsk", @"Merisiel", @"Seoni", @"Valeros"];
}

- (NSArray *)availableCharacters {
    return [PACCharacterManager availableCharacters];
}

- (NSArray *)roleCardNames {
    return [self.character.roleCards valueForKey:@"name"];
}

- (NSRegularExpression *)featRegex {
    if ( _featRegex == nil ) {
        NSError *error = nil;
        _featRegex = [[NSRegularExpression alloc] initWithPattern:@"%(.*?):?([^:%]+)%" options:0 error:&error];
        NSAssert(_featRegex, @"Failed to create featRegex error, %@", error);
    }
    return _featRegex;
}

- (NSRegularExpression *)whitespaceCleaningRegex {
    if ( _whitespaceCleaningRegex == nil ) {
        NSError *error = nil;
        _whitespaceCleaningRegex = [[NSRegularExpression alloc] initWithPattern:@"( +)([ ,.])" options:0 error:&error];
        NSAssert(_whitespaceCleaningRegex, @"Failed to create featRegex error, %@", error);
    }
    return _whitespaceCleaningRegex;
}

- (NSDictionary *)objectFromJSONFile:(NSString *)jsonFile error:(NSError**)error {
    NSString *path = [[NSBundle mainBundle] pathForResource:jsonFile ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *ret = [NSJSONSerialization JSONObjectWithData:data options:0 error:error];
    return ret;
}

- (void)populateBasicCharacterFromJSONFile:(NSString *)jsonFile {
    NSManagedObjectContext *context = self.context;
    if (self.character) {
        [self.context deleteObject:self.character];
        self.character = nil;
    }
    NSDictionary *data = [self objectFromJSONFile:jsonFile error:nil];
    PACCharacter *character = [PACCharacter instanceInManagedObjectContext:context];

    character.name = data[@"name"];
    character.job = data[@"class"];
    character.gender = data[@"gender"];
    character.race = data[@"race"];
    character.favoredCardType = data[@"favoredCardType"];

    NSArray *skills = data[@"skills"];
    [skills enumerateObjectsUsingBlock:^(NSDictionary *skillDict, NSUInteger idx, BOOL *stop) {
        PACSkillProperty *skill = [PACSkillProperty instanceInManagedObjectContext:context];
        [self populateProperty:skill fromDict:skillDict];
        skill.character = character;
        NSArray *specialSkills = skillDict[@"specialSkills"];
        [specialSkills enumerateObjectsUsingBlock:^(NSDictionary *specialDict, NSUInteger idx, BOOL *stop) {
            PACSpecialtySkillProperty *specialty = [PACSpecialtySkillProperty instanceInManagedObjectContext:context];
            [self populateProperty:specialty fromDict:specialDict];
            specialty.effect = [skill.name stringByAppendingString:specialty.effect];
            specialty.baseSkill = skill;
            specialty.character = character;
        }];
    }];

    NSArray *powers = data[@"powers"];
    [powers enumerateObjectsUsingBlock:^(NSDictionary *powerDict, NSUInteger idx, BOOL *stop) {
        PACPowerProperty *power = [PACPowerProperty instanceInManagedObjectContext:context];
        [self populateProperty:power fromDict:powerDict];
        power.character = character;
    }];

    NSArray *cardsList = data[@"cardsList"];
    [cardsList enumerateObjectsUsingBlock:^(NSDictionary *cardDict, NSUInteger idx, BOOL *stop) {
        PACCardListProperty *cardList = [PACCardListProperty instanceInManagedObjectContext:context];
        [self populateProperty:cardList fromDict:cardDict];
        cardList.character = character;
    }];

    self.character = character;

    NSArray *roleArray = data[@"roleCards"];
    [roleArray enumerateObjectsUsingBlock:^(NSDictionary *roleCardDict, NSUInteger idx, BOOL *stop) {
        PACRoleCard *roleCard = [PACRoleCard instanceInManagedObjectContext:context];
        roleCard.name = roleCardDict[@"name"];
        roleCard.data = [NSJSONSerialization dataWithJSONObject:roleCardDict options:0 error:nil];
        roleCard.character = character;
    }];
}

- (void)applyRoleCardWithName:(NSString *)name {
    PACCharacter *character = self.character;
    if (character.roleAdded) {
        return;
    }
    PACRoleCard *roleCard = [self roleCardWithName:name];
    if (roleCard == nil) {
        return;
    }

    character.job = name;

    NSDictionary *roleCardDict = [NSJSONSerialization JSONObjectWithData:roleCard.data options:0 error:nil];

    [roleCardDict[@"powers"] enumerateObjectsUsingBlock:^(NSDictionary *power, NSUInteger idx, BOOL *stop) {
        NSString *configId = power[@"id"];
        PACCharacterProperty *property = [self propertyWithConfigId:configId];
        if (property == nil) {
            property = [PACPowerProperty instanceInManagedObjectContext:self.context];
            property.character = character;
        }
        [self populateProperty:property fromDict:power];
    }];
    character.roleAdded = YES;
}

- (PACRoleCard *)roleCardWithName:(NSString *)name {
    __block PACRoleCard *found = nil;
    [self.character.roleCards enumerateObjectsUsingBlock:^(PACRoleCard *roleCard, NSUInteger idx, BOOL *stop) {
        if ([roleCard.name isEqualToString:name]) {
            found = roleCard;
            *stop = YES;
        }
    }];
    return found;
}

- (PACCharacterProperty *)propertyWithConfigId:(NSString *)configId {
    __block PACCharacterProperty *found = nil;
    [self.character.properties enumerateObjectsUsingBlock:^(PACCharacterProperty *property, NSUInteger idx, BOOL *stop) {
        if ([property.configId isEqualToString:configId]) {
            found = property;
            *stop = YES;
        }
    }];
    return found;
}

- (void)populateProperty:(PACCharacterProperty *)property fromDict:(NSDictionary *)dict {
    NSManagedObjectContext *context = self.context;
    property.name = dict[@"name"];
    property.configId = dict[@"id"];
    property.optional = [dict[@"optional"] boolValue];
    NSString *effect = dict[@"effect"];
    NSArray *matches = [self.featRegex matchesInString:effect options:0 range:NSMakeRange(0, [effect length])];
    [matches enumerateObjectsUsingBlock:^(NSTextCheckingResult *result, NSUInteger idx, BOOL *stop) {
        NSRange defaultValueRange = [result rangeAtIndex:1];
        NSRange configIdRange = [result rangeAtIndex:2];
        NSString *defaultValue = defaultValueRange.length == 0 ? nil : [effect substringWithRange:defaultValueRange];
        NSString *configId = [effect substringWithRange:configIdRange];
        if (defaultValue) {
            PACCharacterFeat *feat = [PACCharacterFeat instanceInManagedObjectContext:context];
            feat.defaultValue = YES;
            feat.value = defaultValue;
            feat.configId = configId ? configId : @"feat";
            feat.active = YES;
            feat.property = property;
        }
    }];
    NSArray *feats = dict[@"feats"];
    if ([matches count] == 0) {
        if ([feats count] > 0) {
            property.effect = [effect stringByAppendingString:@"%feat%"];
        } else {
            property.effect = effect;
        }
    } else {
        property.effect = [self.featRegex stringByReplacingMatchesInString:effect
                                                                   options:0
                                                                     range:NSMakeRange(0, [effect length])
                                                              withTemplate:@"%$2%"];
    }

    [feats enumerateObjectsUsingBlock:^(NSDictionary *featDict, NSUInteger idx, BOOL *stop) {
        NSArray *values = [featDict[@"value"] componentsSeparatedByString:@"|"];
        [values enumerateObjectsUsingBlock:^(NSString *value, NSUInteger idx, BOOL *stop) {
            PACCharacterFeat *feat = [PACCharacterFeat instanceInManagedObjectContext:context];
            feat.value = value;
            NSString *configId = featDict[@"id"];
            feat.configId = configId ? configId : @"feat";
            feat.active = [featDict[@"active"] boolValue];
            feat.property = property;
        }];
    }];
}


#pragma mark - output descriptions

- (NSString *)compactDescriptionOfCharacter {
    return [self characterDescriptionWithPropertyDescriptionBlock:^(PACCharacterProperty *property) {
        return [self compactDescriptionOfProperty:property];
    }];
}

- (NSString *)expandedDescriptionOfCharacter {
    return [self characterDescriptionWithPropertyDescriptionBlock:^(PACCharacterProperty *property) {
        return [self expandedDescriptionOfProperty:property];
    }];
}

- (NSString *)characterDescriptionWithPropertyDescriptionBlock:(NSString *(^)(PACCharacterProperty *))propertyBlock {
    NSMutableArray *lines = [[NSMutableArray alloc] init];
    [lines addObject:[self basicCharacterDescription]];

    NSMutableArray *skills = [[NSMutableArray alloc] init];
    NSMutableArray *powers = [[NSMutableArray alloc] init];
    NSMutableArray *cardList = [[NSMutableArray alloc] init];
    [self.character.properties enumerateObjectsUsingBlock:^(PACCharacterProperty *property, NSUInteger idx, BOOL *stop) {
        NSString *propertyDescription = propertyBlock(property);
        if (propertyDescription) {
            if ([property isMemberOfClass:[PACSkillProperty class]]) {
                [skills addObject:propertyDescription];
            } else if ([property isMemberOfClass:[PACSpecialtySkillProperty class]]) {
                [skills addObject:[NSString stringWithFormat:@"  %@", propertyDescription]];
            } else if ([property isMemberOfClass:[PACPowerProperty class]]) {
                [powers addObject:propertyDescription];
            } else if ([property isMemberOfClass:[PACCardListProperty class]]) {
                [cardList addObject:propertyDescription];
            }
        }
    }];

    [lines addObject:@"SKILLS"];
    [lines addObjectsFromArray:skills];

    [lines addObject:@"POWERS"];
    [lines addObjectsFromArray:powers];

    [lines addObject:[NSString stringWithFormat:@"CARDS LIST - Favored Card Type: %@", self.character.favoredCardType]];
    [lines addObjectsFromArray:cardList];

    NSString *ret = [lines componentsJoinedByString:@"\n"];
    return [self.whitespaceCleaningRegex stringByReplacingMatchesInString:ret
                                                                  options:0
                                                                    range:NSMakeRange(0, [ret length])
                                                             withTemplate:@"$2"];
}

- (NSString *)basicCharacterDescription {
    PACCharacter *character = self.character;
    return [NSString stringWithFormat:@"%@ - %@ %@ %@", character.name, character.gender, character.race, character.job];
}

- (NSString *)compactDescriptionOfProperty:(PACCharacterProperty *)property {
    return [PACCharacterManager compactDescriptionOfProperty:property];
}

+ (NSString *)compactDescriptionOfProperty:(PACCharacterProperty *)property {
    // Optional proprety not selected
    if (property.optional && !property.active) {
        return nil;
    }

    NSArray *feats = [property.feats array];
    NSMutableDictionary *featMap = nil;
    if ([feats count] > 0) {
        featMap = [[NSMutableDictionary alloc] init];
        [feats enumerateObjectsUsingBlock:^(PACCharacterFeat *feat, NSUInteger idx, BOOL *stop) {
            NSString *configId = feat.configId;
            if (!featMap[configId]) {
                featMap[configId] = @"";
            }
            if (feat.defaultValue || feat.active) {
                featMap[configId] = feat.value;
            }
        }];
    }

    NSMutableString *description = [property.effect mutableCopy];
    [featMap enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
        [description replaceOccurrencesOfString:[NSString stringWithFormat:@"%%%@%%", key] withString:value options:0 range:NSMakeRange(0, [description length])];
    }];
    NSString *mainDescription;
    if (property.name) {
        mainDescription = [NSString stringWithFormat:@"%@: %@", property.name, description];
    } else {
        mainDescription = description;
    }

    if ([property isMemberOfClass:[PACSkillProperty class]]) {
        PACSkillProperty *skillProperty = (id)property;
        NSMutableArray *items = [[NSMutableArray alloc] init];
        [items addObject:mainDescription];
        [skillProperty.specialSkills enumerateObjectsUsingBlock:^(PACCharacterProperty *specialSkill, NSUInteger idx, BOOL *stop) {
            NSString *specialDesc = [self compactDescriptionOfProperty:specialSkill];
            if (specialDesc) {
                [items addObject:specialDesc];
            }
        }];
        mainDescription = [items componentsJoinedByString:@"\n"];
    }

    return mainDescription;
}

+ (NSAttributedString *)compactAttributedDescriptionOfProperty:(PACCharacterProperty *)property {
    // Optional proprety not selected
    if (property.optional && !property.active) {
        return nil;
    }

    NSArray *feats = [property.feats array];
    NSMutableDictionary *featMap = nil;
    if ([feats count] > 0) {
        featMap = [[NSMutableDictionary alloc] init];
        [feats enumerateObjectsUsingBlock:^(PACCharacterFeat *feat, NSUInteger idx, BOOL *stop) {
            NSString *configId = feat.configId;
            if (!featMap[configId]) {
                featMap[configId] = @"";
            }
            if (feat.defaultValue || feat.active) {
                featMap[configId] = feat.value;
            }
        }];
    }

    NSMutableString *description = [property.effect mutableCopy];
    [featMap enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
        [description replaceOccurrencesOfString:[NSString stringWithFormat:@"%%%@%%", key] withString:value options:0 range:NSMakeRange(0, [description length])];
    }];
    NSMutableAttributedString *mainString = [[NSMutableAttributedString alloc] init];
    if (property.name) {
        [mainString appendAttributedString:[[NSAttributedString alloc] initWithString:property.name
                                                                           attributes:@{
                                                                                        NSFontAttributeName : [UIFont boldSystemFontOfSize:12.0]
                                                                                        }]];
        [mainString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\t%@", description] attributes:@{}]];
    } else {
        [mainString appendAttributedString:[[NSAttributedString alloc] initWithString:description attributes:@{}]];
    }

    if ([property isMemberOfClass:[PACSkillProperty class]]) {
        NSMutableParagraphStyle *skillStyle = [[NSMutableParagraphStyle alloc] init];
        skillStyle.tabStops = @[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:80.0 options:nil]];
        [mainString addAttribute:NSParagraphStyleAttributeName value:skillStyle range:NSMakeRange(0, [mainString length])];
        PACSkillProperty *skillProperty = (id)property;
        NSMutableArray *items = [[NSMutableArray alloc] init];
        [items addObject:@""];
        [skillProperty.specialSkills enumerateObjectsUsingBlock:^(PACCharacterProperty *specialSkill, NSUInteger idx, BOOL *stop) {
            NSString *specialDesc = [self compactDescriptionOfProperty:specialSkill];
            if (specialDesc) {
                [items addObject:specialDesc];
            }
        }];
        NSString *specialSkills = [items componentsJoinedByString:@"\n\t"];
        NSMutableParagraphStyle *specialStyle = [[NSMutableParagraphStyle alloc] init];
        specialStyle.tabStops = @[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:10.0 options:nil]];
        [mainString appendAttributedString:[[NSAttributedString alloc] initWithString:specialSkills
                                                                           attributes:@{
                                                                                        NSParagraphStyleAttributeName : specialStyle
                                                                                        }]];
    } else {
        NSMutableParagraphStyle *otherStyle = [[NSMutableParagraphStyle alloc] init];
        otherStyle.tabStops = @[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:100.0 options:nil]];
        [mainString addAttribute:NSParagraphStyleAttributeName value:otherStyle range:NSMakeRange(0, [mainString length])];
    }

    return mainString;
}

#define CHECKEDBOX @"☑︎"
#define UNCHECKEDBOX @"☐"
#define NBSP @"\u00A0"

- (NSString *)expandedDescriptionOfProperty:(PACCharacterProperty *)property {
    return [[PACCharacterManager expandedAttributedDescriptionOfProperty:property] string];
}

+ (NSAttributedString *)attributedDescriptionOfFeat:(PACCharacterFeat *)feat named:(BOOL)named{
    NSAttributedString *attrString;
    if (feat.defaultValue) {
        if ([feat.value isEqualToString:@"None"]) {
            attrString = [[NSAttributedString alloc] initWithString:@"–"];
        } else {
            attrString = [[NSAttributedString alloc] initWithString:feat.value];
        }
    } else {
        NSMutableAttributedString *start = [[NSMutableAttributedString alloc] initWithString:named ? @"\t" : @""];
        NSString *lead = named ? @"" : @"(";
        NSString *trail = named ? @"" : @")";
        NSString *pad = [feat.value characterAtIndex:0] == '+' ? @"" : NBSP;
        NSString *featString = [NSString stringWithFormat:@"%@%@%@%@%@", lead, feat.active ? CHECKEDBOX : UNCHECKEDBOX, pad, feat.value, trail];
        [start appendAttributedString:[[NSAttributedString alloc] initWithString:featString
                                                                      attributes:@{
                                                                                   @"PACFeat" : feat,
                                                                                   @"PACFeatName" : feat.configId,
                                                                                   @"PACFeatValue" : feat.value}]];
        attrString = start;
    }
    return attrString;
}

+ (NSAttributedString *)expandedAttributedDescriptionOfProperty:(PACCharacterProperty *)property {
    NSMutableArray *components = [[NSMutableArray alloc] init];

    BOOL applyOptionalUncheckedToEntireResult = NO;
    if (property.optional) {
        NSAttributedString *propertyCheckbox;
        if (property.active) {
            propertyCheckbox = [[NSAttributedString alloc] initWithString:CHECKEDBOX NBSP
                                                               attributes:@{
                                                                            @"PACOptionalChecked" : @YES,
                                                                            NSFontAttributeName : [UIFont systemFontOfSize:18.0]
                                                                            }];
        } else {
            applyOptionalUncheckedToEntireResult = YES;
            propertyCheckbox = [[NSAttributedString alloc] initWithString:UNCHECKEDBOX NBSP
                                                               attributes:@{
                                                                            @"PACOptionalChecked" : @NO,
                                                                            NSFontAttributeName : [UIFont systemFontOfSize:18.0]
                                                                            }];
        }
        [components addObject:propertyCheckbox];
    }

    if (property.name) {
        NSAttributedString *nameString;
        nameString = [[NSAttributedString alloc] initWithString:property.name
                                                     attributes:@{
                                                                  NSFontAttributeName : [UIFont boldSystemFontOfSize:12.0],
                                                                  @"PACBaselineAdjust" : @(-2.0)
                                                                  }];
        [components addObject:nameString];
        [components addObject:[[NSAttributedString alloc] initWithString:@"\t"]];
    }

    NSArray *feats = [property.feats array];
    NSMutableDictionary *featMap = nil;
    if ([feats count] > 0) {
        featMap = [[NSMutableDictionary alloc] init];
        [feats enumerateObjectsUsingBlock:^(PACCharacterFeat *feat, NSUInteger idx, BOOL *stop) {
            NSString *configId = feat.configId;
            if (!featMap[configId]) {
                featMap[configId] = [[NSMutableAttributedString alloc] init];
            }
            [featMap[configId] appendAttributedString:[self attributedDescriptionOfFeat:feat named:(property.name != nil)]];
        }];
    }

    NSMutableAttributedString *attrDescription = [[NSMutableAttributedString alloc] initWithString:property.effect];
    [featMap enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSAttributedString *feats, BOOL *stop) {
        NSRange placeholderRange = NSMakeRange(0, 0);
        NSString *placeholder = [NSString stringWithFormat:@"%%%@%%", key];
        while (placeholderRange.location != NSNotFound) {
            placeholderRange = [attrDescription.string rangeOfString:placeholder];
            if (placeholderRange.location != NSNotFound) {
                [attrDescription replaceCharactersInRange:placeholderRange withAttributedString:feats];
            }
        }
    }];

    [attrDescription addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18.0] range:NSMakeRange(0, [attrDescription length])];

    [components addObject:attrDescription];

    NSMutableAttributedString *result = [[NSMutableAttributedString alloc] init];
    [components enumerateObjectsUsingBlock:^(NSAttributedString *component, NSUInteger idx, BOOL *stop) {
        [result appendAttributedString:component];
    }];

    if ([property isMemberOfClass:[PACSkillProperty class]]) {
        NSMutableParagraphStyle *skillStyle = [[NSMutableParagraphStyle alloc] init];
        skillStyle.tabStops = @[
                                [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:80.0 options:nil],
                                [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:140.0 options:nil],
                                [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:180.0 options:nil],
                                [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:220.0 options:nil],
                                [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:260.0 options:nil]
                                ];
        [result addAttribute:NSParagraphStyleAttributeName value:skillStyle range:NSMakeRange(0, [result length])];
        PACSkillProperty *skillProperty = (id)property;
        NSMutableArray *items = [[NSMutableArray alloc] init];
        [items addObject:@""];
        [skillProperty.specialSkills enumerateObjectsUsingBlock:^(PACCharacterProperty *specialSkill, NSUInteger idx, BOOL *stop) {
            NSString *specialDesc = [self compactDescriptionOfProperty:specialSkill];
            if (specialDesc) {
                [items addObject:specialDesc];
            }
        }];
        NSString *specialSkills = [items componentsJoinedByString:@"\n\t"];
        NSMutableParagraphStyle *specialStyle = [[NSMutableParagraphStyle alloc] init];
        specialStyle.tabStops = @[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:10.0 options:nil]];
        [result appendAttributedString:[[NSAttributedString alloc] initWithString:specialSkills
                                                                       attributes:@{
                                                                                    NSParagraphStyleAttributeName : specialStyle
                                                                                    }]];
    } else {
        NSMutableParagraphStyle *otherStyle = [[NSMutableParagraphStyle alloc] init];
        otherStyle.tabStops = @[
                                [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:100.0 options:nil],
                                [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:140.0 options:nil],
                                [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:185.0 options:nil],
                                [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:230.0 options:nil],
                                [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:275.0 options:nil]
                                ];
        [result addAttribute:NSParagraphStyleAttributeName value:otherStyle range:NSMakeRange(0, [result length])];
    }

    if (applyOptionalUncheckedToEntireResult) {
        [result addAttribute:@"PACOptionalChecked" value:@NO range:NSMakeRange(0, [result length])];
    }
    return result;
}

@end
