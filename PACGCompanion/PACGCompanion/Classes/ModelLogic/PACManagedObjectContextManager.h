//
//  PACManagedObjectContextManager.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/19/13.
//
//

#import <Foundation/Foundation.h>

@interface PACManagedObjectContextManager : NSObject

@property (nonatomic, strong) NSManagedObjectContext *context;

- (id) init __attribute__((unavailable("init not available")));
- (id)initWithManagedObjectContext:(NSManagedObjectContext *)context;

@end
