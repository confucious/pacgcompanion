//
//  PACCharacterManager.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import "PACManagedObjectContextManager.h"
#import "PACCharacter.h"

@interface PACCharacterManager : PACManagedObjectContextManager

@property (nonatomic, strong) PACCharacter *character;
@property (nonatomic, readonly) NSArray *roleCardNames;
@property (nonatomic, readonly) NSArray *availableCharacters;

+ (NSArray *)availableCharacters;

- (void)populateBasicCharacterFromJSONFile:(NSString *)jsonFile;
- (void)applyRoleCardWithName:(NSString *)name;

- (NSString *)compactDescriptionOfCharacter;
- (NSString *)expandedDescriptionOfCharacter;

+ (NSString *)compactDescriptionOfProperty:(PACCharacterProperty *)property;
+ (NSAttributedString *)compactAttributedDescriptionOfProperty:(PACCharacterProperty *)property;
+ (NSAttributedString *)expandedAttributedDescriptionOfProperty:(PACCharacterProperty *)property;

@end
