//
//  PACCampaignManager.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/19/13.
//
//

#import "PACCampaignManager.h"
#import "PACModels.h"

@implementation PACCampaignManager

- (NSArray *)savedCampaigns {
    NSManagedObjectContext *context = self.context;
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"PACCampaign"];
    [fetch setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"createdOn" ascending:NO]]];
    NSError *error;
    NSArray *ret = [context executeFetchRequest:fetch error:&error];
    NSAssert(ret, @"Failed to load savedCampaigns, error: %@", error);
    return ret;
}

- (void)deleteCampaign:(PACCampaign *)campaign {
    NSManagedObjectContext *context = self.context;
    [context deleteObject:campaign];
}

- (void)touchCampaign:(PACCampaign *)campaign {
    campaign.modifiedOn = [NSDate timeIntervalSinceReferenceDate];
    [[campaign managedObjectContext] save:nil];
}

- (PACCampaign *)createEmptyCampaign {
    PACCampaign *campaign = [PACCampaign instanceInManagedObjectContext:self.context];
    campaign.createdOn = [NSDate timeIntervalSinceReferenceDate];
    return campaign;
}

@end
