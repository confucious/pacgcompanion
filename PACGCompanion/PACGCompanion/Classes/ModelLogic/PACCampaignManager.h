//
//  PACCampaignManager.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/19/13.
//
//

/*
 * Methods to retrieve/create campaign objects.
 */

#import "PACManagedObjectContextManager.h"
#import "PACCampaign.h"

@interface PACCampaignManager : PACManagedObjectContextManager

- (NSArray *)savedCampaigns;
- (void)deleteCampaign:(PACCampaign *)campaign;
- (void)touchCampaign:(PACCampaign *)campaign;
- (PACCampaign *)createEmptyCampaign;

@end
