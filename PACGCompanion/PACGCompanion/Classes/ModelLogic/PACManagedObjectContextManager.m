//
//  PACManagedObjectContextManager.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/19/13.
//
//

#import "PACManagedObjectContextManager.h"

@implementation PACManagedObjectContextManager

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)context {
    self = [super init];
    if (self) {
        _context = context;
    }
    return self;
}

@end
