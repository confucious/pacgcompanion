//
//  PACCampaignViewController.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

#import "PACCampaignViewController.h"
#import "UIViewController+CoreData.h"
#import "PACLoadCharacterViewController.h"
#import "PACCharacterViewController.h"
#import "PACDeviceInfo.h"
#import "PACCollectionViewCoverFlowLayout.h"
#import "LXReorderableCollectionViewFlowLayout.h"

// Debug import
#import "PACCharacterManager.h"

@interface PACCampaignViewController () <
PACLoadCharacterViewControllerDelegate,
UICollectionViewDataSource,
UICollectionViewDelegate
>

@property (nonatomic, strong) PACCampaign *campaign;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *viewControllers;

@end

@implementation PACCampaignViewController

- (id)initWithCampaign:(PACCampaign *)campaign inContext:(NSManagedObjectContext *)context {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.managedObjectContext = context;
        _campaign = campaign;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    UICollectionViewLayout *layout = [self collectionViewLayout];
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    self.collectionView = collectionView;
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellId"];
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    CGFloat eyePosition = 500.0;
    CATransform3D perspective = CATransform3DIdentity;
    perspective.m34 = -1.0/eyePosition;
    collectionView.layer.sublayerTransform = perspective;

    [self.view addSubview:collectionView];

    NSManagedObjectContext *tempContext = [[NSManagedObjectContext alloc] init];
    [tempContext setParentContext:self.managedObjectContext];
    PACCharacterManager *manager = [[PACCharacterManager alloc] initWithManagedObjectContext:tempContext];
    [manager populateBasicCharacterFromJSONFile:@"Seoni"];
    PACCharacter *character1 = manager.character;
    manager = [[PACCharacterManager alloc] initWithManagedObjectContext:tempContext];
    [manager populateBasicCharacterFromJSONFile:@"Valeros"];
    PACCharacter *character2 = manager.character;
    manager = [[PACCharacterManager alloc] initWithManagedObjectContext:tempContext];
    [manager populateBasicCharacterFromJSONFile:@"Ezren"];
    PACCharacter *character3 = manager.character;
    manager = [[PACCharacterManager alloc] initWithManagedObjectContext:tempContext];
    [manager populateBasicCharacterFromJSONFile:@"Harsk"];
    PACCharacter *character4 = manager.character;
    manager = [[PACCharacterManager alloc] initWithManagedObjectContext:tempContext];
    [manager populateBasicCharacterFromJSONFile:@"Merisiel"];
    PACCharacter *character5 = manager.character;
    NSError *error = nil;
    if (![tempContext save:&error]) {
        NSLog(@"Error: %@", error);
    }

    self.viewControllers = @[
                             [[PACCharacterViewController alloc] initWithCharacter:character1 inContext:tempContext],
                             [[PACCharacterViewController alloc] initWithCharacter:character2 inContext:self.managedObjectContext],
                             [[PACCharacterViewController alloc] initWithCharacter:character3 inContext:self.managedObjectContext],
                             [[PACCharacterViewController alloc] initWithCharacter:character4 inContext:self.managedObjectContext],
                             [[PACCharacterViewController alloc] initWithCharacter:character5 inContext:self.managedObjectContext],
//                             [[PACCharacterViewController alloc] initWithCharacter:nil inContext:self.managedObjectContext],
//                             [[PACCharacterViewController alloc] initWithCharacter:nil inContext:self.managedObjectContext],
//                             [[PACCharacterViewController alloc] initWithCharacter:nil inContext:self.managedObjectContext],
//                             [[PACCharacterViewController alloc] initWithCharacter:nil inContext:self.managedObjectContext]
                             ];

}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if (!iPad) {
        CGFloat inset = (self.view.bounds.size.width - 320.0) / 2.0;
        self.collectionView.contentInset = UIEdgeInsetsMake(20.0, inset, 0.0, inset);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)presentLoadCharacterVC {
    PACLoadCharacterViewController *con = [[PACLoadCharacterViewController alloc] initInContext:self.managedObjectContext];
    con.delegate = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:con];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - UICollectionViewLayouts

- (UICollectionViewLayout *)collectionViewLayout {
    if (iPad) {
        LXReorderableCollectionViewFlowLayout *layout = [[LXReorderableCollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(320.0,300.0);
        return layout;
    } else {
        PACCollectionViewCoverFlowLayout *layout = [[PACCollectionViewCoverFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(320.0,300.0);
        return layout;
    }
}

#pragma mark - UICollectionViewDataSource UICollectionViewDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.viewControllers count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PACCharacterViewController *con = self.viewControllers[indexPath.item];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellId" forIndexPath:indexPath];
    [self addChildViewController:con];;
    [cell.contentView addSubview:con.view];
    [con didMoveToParentViewController:self];
    con.view.frame = cell.contentView.bounds;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    PACCharacterViewController *con = self.viewControllers[indexPath.item];
    [con willMoveToParentViewController:nil];
    [con.view removeFromSuperview];
    [con removeFromParentViewController];
}

#pragma mark - PACLoadCharacterViewControllerDelegate

- (void)loadCharacterVC:(PACLoadCharacterViewController *)controller createdCharacter:(PACCharacter *)character {
    [self dismissViewControllerAnimated:YES completion:nil];
    if (character) {
        character.campaign = self.campaign;
        [self.managedObjectContext save:nil];
    }
}

@end
