//
//  PACCampaignListViewController.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/21/13.
//
//

#import <UIKit/UIKit.h>
#import "PACCampaign.h"

@protocol PACCampaignListViewControllerDelegate;

@interface PACCampaignListViewController : UIViewController

@property (nonatomic, weak) id <PACCampaignListViewControllerDelegate> delegate;

- (id) init __attribute__((unavailable("init not available")));
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil __attribute__((unavailable("init not available")));
- (id)initWithContext:(NSManagedObjectContext *)context;

@end

@protocol PACCampaignListViewControllerDelegate <NSObject>

/// campaign is nil if canceled.
- (void)campaignListVC:(PACCampaignListViewController *)controller selectedCampaign:(PACCampaign *)campaign;

@end