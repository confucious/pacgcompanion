//
//  PACCharacterViewController.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

#import "PACCharacterViewController.h"
#import "PACModels.h"
#import "PACSectionRange.h"
#import "UIViewController+CoreData.h"
#import "PACCharacterView.h"
#import "PACCharacterPropertyCellContent.h"
#import "PACColumnCollectionViewLayout.h"
#import "PACColumnCollectionViewDividerView.h"

@interface PACCharacterViewController () <
UICollectionViewDataSource,
UICollectionViewDelegate,
PACColumnCollectionViewLayoutDelegate,
PACCharacterPropertyCellContentDelegate>

@property (nonatomic, strong) PACCharacter *character;
@property (nonatomic, readonly) PACCharacterView *characterView;
@property (nonatomic, strong) NSMutableArray *propertyViews;
@property (nonatomic, strong) NSMutableArray *sectionRanges;
@property (nonatomic, strong) NSMutableArray *headerViews;
@property (nonatomic, strong) PACColumnCollectionViewLayout *compactLayout;
@property (nonatomic, strong) PACColumnCollectionViewLayout *editLayout;
@end

@implementation PACCharacterViewController

#define CARD_WIDTH 320.0
#define SECTION_HEIGHT 20.0

- (id)initWithCharacter:(PACCharacter *)character inContext:(NSManagedObjectContext *)context {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.managedObjectContext = context;
        _character = character;
        self.navigationItem.title = character.name;
        [self configureForNormal];
        [self populateCollectionData];
    }
    return self;
}

- (void)loadView {
    [super loadView];
    self.view = [[PACCharacterView alloc] init];
}

static NSString *kCellReuseId = @"cellReuseId";
static NSString *kHeaderReuseId = @"headerReuseId";
static NSString *kDividerReuseId = @"dividerReuseId";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.25];
    PACCharacterView *view = self.characterView;
    [view.navBar setItems:@[self.navigationItem]];
    [view.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:kCellReuseId];
    [view.collectionView registerClass:[UICollectionViewCell class] forSupplementaryViewOfKind:@"header" withReuseIdentifier:kHeaderReuseId];
    [view.collectionView registerClass:[PACColumnCollectionViewDividerView class] forSupplementaryViewOfKind:@"divider" withReuseIdentifier:kDividerReuseId];
    view.collectionView.dataSource = self;
    view.collectionView.delegate = self;
    self.compactLayout = (id)view.collectionViewLayout;
    self.compactLayout.tag = 0;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (PACCharacterView *)characterView {
    return (id) self.view;
}

- (PACColumnCollectionViewLayout *)editLayout {
    if (_editLayout == nil) {
        _editLayout = [[PACColumnCollectionViewLayout alloc] init];
        _editLayout.tag = 1;
        _editLayout.minimumRowHeight = 44.0;
        _editLayout.verticalCellAlignment = UIControlContentVerticalAlignmentCenter;
    }
    return _editLayout;
}

- (void)configureForEdit {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                          target:self
                                                                                          action:@selector(cancelTapped)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                           target:self
                                                                                           action:@selector(doneTapped)];
}

- (void)configureForNormal {
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                           target:self
                                                                                           action:@selector(editTapped)];
}

- (void)editTapped {
    [self.propertyViews enumerateObjectsUsingBlock:^(PACCharacterPropertyCellContent *cell, NSUInteger idx, BOOL *stop) {
        cell.mode = PACCharacterPropertyCellContentEdit;
        cell.targetWidth = CARD_WIDTH;
        [cell layoutIfNeeded];
        [cell sizeToFit];
    }];
    [self.editLayout invalidateLayout];
    [self.characterView.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    [self.characterView.collectionView setCollectionViewLayout:self.editLayout animated:YES];
    [self configureForEdit];
}

- (void)configureCellsForNormal {
    [self.propertyViews enumerateObjectsUsingBlock:^(PACCharacterPropertyCellContent *cell, NSUInteger idx, BOOL *stop) {
        cell.mode = PACCharacterPropertyCellContentBasic;
        cell.targetWidth = [cell.property isMemberOfClass:[PACSkillProperty class]] ? CARD_WIDTH / 2.0 : CARD_WIDTH;
        [cell layoutIfNeeded];
        [cell sizeToFit];
    }];
}
- (void)configureLayoutForNormal {
    [self configureCellsForNormal];
    [self.compactLayout invalidateLayout];
    [self.characterView.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    [self.characterView.collectionView setCollectionViewLayout:self.compactLayout animated:YES];
}

- (void)cancelTapped {
    [self configureLayoutForNormal];
    [self configureForNormal];
}

- (void)doneTapped {
    [self configureLayoutForNormal];
    [self configureForNormal];
}

#pragma mark -

- (void)populateCollectionData {
    NSArray *properties = [self.character.properties array];
    NSMutableArray *propertyViews = [[NSMutableArray alloc] initWithCapacity: [properties count]];
    NSMutableArray *sectionRanges = [[NSMutableArray alloc] initWithCapacity:3];
    __block NSInteger lastSectionType = -1;
    [properties enumerateObjectsUsingBlock:^(PACCharacterProperty *property, NSUInteger idx, BOOL *stop) {
        NSInteger sectionType = -1;
        if ([property isMemberOfClass:[PACSkillProperty class]]) {
            sectionType = 0;
        } else if ([property isMemberOfClass:[PACPowerProperty class]]) {
            sectionType = 1;
        } else if ([property isMemberOfClass:[PACCardListProperty class]]) {
            sectionType = 2;
        } else {
            // Skip and don't add to propertyViews.
            return;
        }

        if (lastSectionType != sectionType) {
            lastSectionType = sectionType;
            PACSectionRange *newRange = [[PACSectionRange alloc] initWithLocation:[propertyViews count] andLength:1];
            [sectionRanges addObject:newRange];
        } else {
            PACSectionRange *lastRange = [sectionRanges lastObject];
            lastRange.length++;
        }

        PACCharacterPropertyCellContent *cell = [[PACCharacterPropertyCellContent alloc] initWithFrame:CGRectMake(0.0, 0.0, CARD_WIDTH, CGFLOAT_MAX)];
        cell.property = property;
        cell.actionDelegate = self;
        //cell.backgroundColor = [UIColor redColor];
        [propertyViews addObject:cell];
    }];
    self.propertyViews = propertyViews;
    [self configureCellsForNormal];
    self.sectionRanges = sectionRanges;

    NSMutableArray *headerViews = [[NSMutableArray alloc] initWithCapacity:3];
    [@[@"Skills", @"Powers", @"Card List"] enumerateObjectsUsingBlock:^(NSString *title, NSUInteger idx, BOOL *stop) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, CARD_WIDTH, SECTION_HEIGHT)];
        ;
        NSMutableParagraphStyle *favoredStyle = [[NSMutableParagraphStyle alloc] init];
        // We want NSTextAlignmentRight which is 2 on iOS, but there appears to be a bug where CoreText wants non-iPhone values where the value is 1.
        favoredStyle.tabStops = @[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:2.0 options:nil],
                                  [[NSTextTab alloc] initWithTextAlignment:1 location:CARD_WIDTH-2.0 options:nil]];
        NSString *titleString;
        if (idx < 2) {
            titleString = [[NSString stringWithFormat:@"\t%@", title] uppercaseString];
        } else {
            titleString = [[NSString stringWithFormat:@"\t%@\tFavored Card Type: %@", title, self.character.favoredCardType] uppercaseString];
        }
        NSAttributedString *labelString = [[NSAttributedString alloc] initWithString:titleString
                                                                          attributes:@{
                                                                                       NSFontAttributeName : [UIFont boldSystemFontOfSize:11.0],
                                                                                       NSParagraphStyleAttributeName : favoredStyle
                                                                                       }];
        label.attributedText = labelString;
        label.backgroundColor = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.5];
        [headerViews addObject:label];
    }];
    self.headerViews = headerViews;
}

- (PACCharacterPropertyCellContent *)contentForIndexPath:(NSIndexPath *)indexPath {
    NSRange range = [self.sectionRanges[indexPath.section] range];
    return self.propertyViews[range.location + indexPath.item];
}

- (UIView *)headerForSection:(NSInteger)sectionIndex {
    return self.headerViews[sectionIndex];
}

#pragma mark -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.sectionRanges count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.sectionRanges[section] length];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(PACColumnCollectionViewLayout *)collectionViewLayout numberOfColumnsForSectionAtIndex:(NSInteger)section {
    if (collectionViewLayout.tag == 1) {
        return 1;
    }
    if (section == 0) {
        return 2;
    }
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseId forIndexPath:indexPath];
    [cell.contentView addSubview:[self contentForIndexPath:indexPath]];
    //cell.backgroundColor = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.25];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(PACColumnCollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    PACCharacterPropertyCellContent *cell = [self contentForIndexPath:indexPath];
    if (collectionViewLayout.tag == 1) {
        CGSize size = cell.bounds.size;
        size.width = CARD_WIDTH;
        return size;
    } else {
        if ([cell.attributedText length] > 0) {
            CGSize size = cell.bounds.size;
            size.width = CARD_WIDTH;
            if ([cell.property isMemberOfClass:[PACSkillProperty class]]) {
                size.width = CARD_WIDTH / 2.0;
            }
            return size;
        } else {
            return CGSizeZero;
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    [[self contentForIndexPath:indexPath] removeFromSuperview];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:@"header"]) {
        UICollectionViewCell *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:kHeaderReuseId forIndexPath:indexPath];
        [cell.contentView addSubview:[self headerForSection:indexPath.section]];
        return cell;
    } else if ([kind isEqualToString:@"divider"]) {
        return [collectionView dequeueReusableSupplementaryViewOfKind:@"divider" withReuseIdentifier:kDividerReuseId forIndexPath:indexPath];
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(CARD_WIDTH, SECTION_HEIGHT);
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingSupplementaryView:(UICollectionReusableView *)view forElementOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
    if ([elementKind isEqualToString:@"header"]) {
        [[self headerForSection:indexPath.section] removeFromSuperview];
    }
}

#pragma mark - PACCharacterCellDelegate

- (void)characterPropertyCellTappedOptionalCheckbox:(PACCharacterPropertyCellContent *)cell {
    PACCharacterProperty *property = cell.property;
    property.active = !property.active;
    [cell updateTextViewContent];
}

- (void)characterPropertyCell:(PACCharacterPropertyCellContent *)cell tappedFeat:(PACCharacterFeat *)feat {
    feat.active = !feat.active;
    [cell updateTextViewContent];
}

@end
