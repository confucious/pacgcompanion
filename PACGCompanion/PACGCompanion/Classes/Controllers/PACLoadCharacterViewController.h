//
//  PACLoadCharacterViewController.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/19/13.
//
//

#import <UIKit/UIKit.h>
#import "PACCharacter.h"

@protocol PACLoadCharacterViewControllerDelegate;

@interface PACLoadCharacterViewController : UIViewController

@property (nonatomic, weak) id <PACLoadCharacterViewControllerDelegate> delegate;

- (id) init __attribute__((unavailable("init not available")));
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil __attribute__((unavailable("init not available")));
- (id)initInContext:(NSManagedObjectContext *)context;

@end

@protocol PACLoadCharacterViewControllerDelegate <NSObject>

/// character will be nil if the operation is cancelled.
- (void)loadCharacterVC:(PACLoadCharacterViewController *)controller createdCharacter:(PACCharacter *)character;

@end
