//
//  PACStartViewController.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

/*
 * Startup screen.
 * Load existing campaign, start new campaign.
 */

#import <UIKit/UIKit.h>
#import "UIViewController+CoreData.h"

@interface PACStartViewController : UIViewController

@end
