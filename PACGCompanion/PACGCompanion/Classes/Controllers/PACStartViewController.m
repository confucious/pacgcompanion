//
//  PACStartViewController.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

#import "PACStartViewController.h"
#import "PACCampaignManager.h"
#import "PACStartView.h"
#import "PACCampaignListViewController.h"
#import "PACCampaignViewController.h"

@interface PACStartViewController () <
PACCampaignListViewControllerDelegate>

@property (nonatomic, readonly) PACStartView *startView;
@property (nonatomic, strong) PACCampaignManager *manager;
@property (nonatomic, strong) NSArray *savedCampaigns;

@end

@implementation PACStartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)loadView {
    self.view = [[PACStartView alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    PACStartView *startView = self.startView;
    [startView.resumeCampaignButton addTarget:self action:@selector(resumeTapped) forControlEvents:UIControlEventTouchUpInside];
    [startView.startCampaignButton addTarget:self action:@selector(startTapped) forControlEvents:UIControlEventTouchUpInside];

    self.manager = [[PACCampaignManager alloc] initWithManagedObjectContext:self.managedObjectContext];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.startView.resumeCampaignButton.enabled = [self.savedCampaigns count] > 0;
}

- (PACStartView *)startView {
    return (id)self.view;
}

- (NSArray *)savedCampaigns {
    if (_savedCampaigns == nil) {
        _savedCampaigns = [self.manager savedCampaigns];
    }
    return _savedCampaigns;
}

- (void)resumeTapped {
    PACCampaignListViewController *con = [[PACCampaignListViewController alloc] initWithContext:self.managedObjectContext];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:con];
    con.delegate = self;
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)startTapped {
    PACCampaign *campaign = [self.manager createEmptyCampaign];
    PACCampaignViewController *con = [[PACCampaignViewController alloc] initWithCampaign:campaign inContext:self.managedObjectContext];
    [self presentViewController:con animated:YES completion:nil];
}

- (void)campaignListVC:(PACCampaignListViewController *)controller selectedCampaign:(PACCampaign *)campaign {
    [self dismissViewControllerAnimated:YES completion:^{
        if (campaign) {
            PACCampaignViewController *con = [[PACCampaignViewController alloc] initWithCampaign:campaign inContext:self.managedObjectContext];
            [self presentViewController:con animated:YES completion:nil];
        }
    }];
}

@end
