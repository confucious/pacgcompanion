//
//  UIViewController+CoreData.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

#import "UIViewController+CoreData.h"
#import <objc/runtime.h>

static char const * const m_managedObjectContextKey = "managedObjectContext";

@implementation UIViewController (CoreData)

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = objc_getAssociatedObject(self, m_managedObjectContextKey);
    if (context == nil) {
        // Try to find it on the parent.
        if ([self.parentViewController respondsToSelector:@selector(managedObjectContext)]) {
            context = self.parentViewController.managedObjectContext;
            self.managedObjectContext = context;
        }
    }
    return context;
}

- (void)setManagedObjectContext:(NSManagedObjectContext *)context {
    objc_setAssociatedObject(self, m_managedObjectContextKey, context, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
