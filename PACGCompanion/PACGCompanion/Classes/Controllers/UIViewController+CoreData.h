//
//  UIViewController+CoreData.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (CoreData)

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
