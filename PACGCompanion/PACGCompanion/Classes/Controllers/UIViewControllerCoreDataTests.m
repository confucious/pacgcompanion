//
//  UIViewControllerCoreDataTests.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

#import <XCTest/XCTest.h>
#import "UIViewController+CoreData.h"

@interface UIViewControllerCoreDataTests : XCTestCase

@end

@implementation UIViewControllerCoreDataTests

- (void)setUp {
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testCoreDataSetAndGet {
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] init];
    UIViewController *con1 = [[UIViewController alloc] init];
    con1.managedObjectContext = context;
//    XCTAssertEqual(con1.managedObjectContext, context, @"storage didn't work properly");

    con1.managedObjectContext = nil;
//    XCTAssertNil(con1.managedObjectContext, @"clearing didn't work properly");
}

- (void)testCoreDataParentSearch {
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] init];
    UIViewController *con1 = [[UIViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:con1];
    XCTAssertEqual(con1.parentViewController, nav, @"nav wasn't parent of con1");

    nav.managedObjectContext = context;
    XCTAssertEqual(con1.managedObjectContext, context, @"con1.managedObjectContext didn't retrieve from nav");

    nav.managedObjectContext = nil;
    XCTAssertEqual(con1.managedObjectContext, context, @"con1.managedObjectContext didn't cache result from nav");
}

@end
