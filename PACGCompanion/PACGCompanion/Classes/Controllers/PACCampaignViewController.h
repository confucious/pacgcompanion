//
//  PACCampaignViewController.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

/*
 * Container VC to show characters in campaign.
 * iPhone, swipe between.
 * iPad, show all 6 with tap to zoom.
 */

#import <UIKit/UIKit.h>
#import "PACCampaign.h"

@interface PACCampaignViewController : UIViewController

- (id) init __attribute__((unavailable("init not available")));
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil __attribute__((unavailable("init not available")));
- (id)initWithCampaign:(PACCampaign *)campaign inContext:(NSManagedObjectContext *)context;

@end
