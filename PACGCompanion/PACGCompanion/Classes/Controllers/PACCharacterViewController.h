//
//  PACCharacterViewController.h
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/18/13.
//
//

#import <UIKit/UIKit.h>
#import "PACCharacter.h"

/*
 * Show / edit individual character.
 */

@interface PACCharacterViewController : UIViewController

- (id) init __attribute__((unavailable("init not available")));
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil __attribute__((unavailable("init not available")));
- (id)initWithCharacter:(PACCharacter *)character inContext:(NSManagedObjectContext *)context;

@end
