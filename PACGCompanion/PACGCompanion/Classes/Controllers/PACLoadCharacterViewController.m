//
//  PACLoadCharacterViewController.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/19/13.
//
//

#import "PACLoadCharacterViewController.h"
#import "UIViewController+CoreData.h"
#import "PACCharacterManager.h"
#import "PACLoadCharacterView.h"

@interface PACLoadCharacterViewController () <
UITableViewDataSource,
UITableViewDelegate>

@property (nonatomic, strong) PACCharacterManager *manager;
@property (nonatomic, strong) PACLoadCharacterView *loadCharacterView;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation PACLoadCharacterViewController

- (id)initInContext:(NSManagedObjectContext *)context {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.title = @"Create Character";
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTapped)];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveTapped)];
        self.managedObjectContext = context;
        _manager = [[PACCharacterManager alloc] initWithManagedObjectContext:context];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.loadCharacterView = [[PACLoadCharacterView alloc] init];
    self.loadCharacterView.frame = self.view.bounds;
    [self.loadCharacterView.playerName addTarget:self action:@selector(returnKeyTapped) forControlEvents:UIControlEventEditingDidEnd];

    self.loadCharacterView.tableView.dataSource = self;
    self.loadCharacterView.tableView.delegate = self;
    [self.loadCharacterView.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellId"];
    [self.view addSubview:self.loadCharacterView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button callbacks

- (void)returnKeyTapped {
    [self.view endEditing:NO];
}

- (void)cancelTapped {
    [self.delegate loadCharacterVC:self createdCharacter:nil];
}

- (void)saveTapped {
    [self.manager populateBasicCharacterFromJSONFile:self.manager.availableCharacters[self.selectedIndexPath.row]];
    PACCharacter *character = self.manager.character;
    character.createdBy = self.loadCharacterView.playerName.text;
    character.createdOn = [NSDate timeIntervalSinceReferenceDate];
    [self.delegate loadCharacterVC:self createdCharacter:character];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.manager.availableCharacters count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellId"];
    cell.textLabel.text = self.manager.availableCharacters[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.selectedIndexPath) {
        UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:self.selectedIndexPath];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
    }
    self.selectedIndexPath = indexPath;
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:self.selectedIndexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

@end
