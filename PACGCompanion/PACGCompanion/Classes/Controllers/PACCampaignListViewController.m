//
//  PACCampaignListViewController.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/21/13.
//
//

#import "PACCampaignListViewController.h"
#import "UIViewController+CoreData.h"
#import "PACCampaignManager.h"

@interface PACCampaignListViewController () <
UITableViewDataSource,
UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) PACCampaignManager *manager;
@property (nonatomic, strong) NSArray *savedCampaigns;

@end

@implementation PACCampaignListViewController

- (id)initWithContext:(NSManagedObjectContext *)context {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.managedObjectContext = context;
        self.manager = [[PACCampaignManager alloc] initWithManagedObjectContext:context];
        self.title = @"Resume Campaign";
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                              target:self
                                                                                              action:@selector(cancelTapped)];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    tableView.dataSource = self;
    tableView.delegate = self;
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellId"];
    self.tableView = tableView;
    [self.view addSubview:tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray *)savedCampaigns {
    if (_savedCampaigns == nil) {
        _savedCampaigns = [self.manager savedCampaigns];
    }
    return _savedCampaigns;
}

- (void)cancelTapped {
    [self.delegate campaignListVC:self selectedCampaign:nil];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.savedCampaigns count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellId" forIndexPath:indexPath];
    PACCampaign *campaign = self.savedCampaigns[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"Campaign created on %@", [NSDate dateWithTimeIntervalSinceReferenceDate:campaign.createdOn]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate campaignListVC:self selectedCampaign:self.savedCampaigns[indexPath.row]];
}

@end
