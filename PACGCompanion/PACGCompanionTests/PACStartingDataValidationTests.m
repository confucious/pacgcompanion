//
//  PACStartingDataValidationTests.m
//  PACGCompanion
//
//  Created by Jerry Hsu on 12/10/13.
//
//

#import <XCTest/XCTest.h>
#import "PACCharacterManager.h"

@interface PACStartingDataValidationTests : XCTestCase

@end

@implementation PACStartingDataValidationTests

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testCharacterJsonFiles {
//    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
    NSArray *testFiles = [PACCharacterManager availableCharacters];
    [testFiles enumerateObjectsUsingBlock:^(NSString *jsonFile, NSUInteger idx, BOOL *stop) {
        NSString *testPath = [[NSBundle bundleForClass:[self class]] pathForResource:jsonFile ofType:@"json"];
        NSData *jsonData = [NSData dataWithContentsOfFile:testPath];
        NSError *error;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
        XCTAssertNotNil(jsonDict, @"JSON deserialization for file failed %@ with error %@", testPath, error);
    }];
}

@end
